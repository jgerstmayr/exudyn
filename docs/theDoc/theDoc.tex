\documentclass[11pt,a4paper]{book} %print
%\documentclass[12pt,a5paper,landscape]{book} %slides

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

\usepackage{amsmath}
\usepackage{pxfonts} 
%\usepackage{xcolor} %for colors
%\usepackage{floatrow} %?
\usepackage{graphicx} %\includegraphics

\usepackage{import}
\usepackage{hyperref}
\usepackage{longtable} %for tables in reference manual

\usepackage[latin1]{inputenc} %Achtung, dies ist die File-Encodierung (wird meist als ANSI angezeigt) und sollte nicht auf UTF-8 stehen (in Notepad++ umstellen!)
\usepackage[T1]{fontenc}

%\input{docincludes}
\usepackage{docincludes}
\usepackage{xcolor}

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% include python codes:
% Default fixed font does not support bold face
\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{9} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{9}  % for normal

% Custom colors
\usepackage{color}
\definecolor{deepblack}{rgb}{0,0,0}
\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0.4}
\definecolor{deepgreen}{rgb}{0,0.5,0}
\definecolor{commentgreen}{rgb}{0.5,0.6,0.5}

\usepackage{listings}

% Python style for highlighting example codes:
\newcommand\pythonstyle{\lstset{
language=Python,
basicstyle=\ttm,
otherkeywords={self},             % Add keywords here
keywordstyle=\ttb\color{deepblue},
emph={AddNode, AddObject, AddMarker, AddLoad, SystemContainer, AddSystem, Assemble, SimulationSettings,
TimeIntegrationSolve,__init__},          % Custom highlighting
emphstyle=\ttb\color{deepred},    % Custom highlighting style
stringstyle=\color{deepgreen},
commentstyle=\color{commentgreen},
frame=tb,                         % Any extra options here
showstringspaces=false,           % 
numbers=left,										 %show line numbers
breaklines=true,									 %automatically breaks lines
numberstyle=\ttm    						% line number style
}}
%plain listing style:
\newcommand\plainlststyle{\lstset{
language=Python,
basicstyle=\ttm,
otherkeywords={self},             % Add keywords here
keywordstyle=\ttm,
emphstyle=\ttm,    % Custom highlighting style
stringstyle=\color{deepblack},
commentstyle=\color{deepblack},
frame=tb,                         % Any extra options here
showstringspaces=false,           % 
numbers=none,										 %show line numbers
breaklines=true,									 %automatically breaks lines
numberstyle=\ttm    						% line number style
}}
% Python for external files
\newcommand\pythonexternal[2][]{{
\pythonstyle
\lstinputlisting[#1]{#2}}}

\newcommand\pythoninline[1]{{\pythonstyle\lstinline!#1!}}


\definecolor{steelblue}{HTML}{307BD0}

\newcommand{\bigmr}[9]{\left[\!\! \begin{array}{ccc} \displaystyle #1 & \displaystyle #2 & \displaystyle #3 \vspace{0.3cm}\\ \displaystyle #4 & \displaystyle #5 & \displaystyle #6 \vspace{0.3cm}\\ \displaystyle #7 & \displaystyle #8 & \displaystyle #9  \end{array} \!\!\right]}

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%page setup:
\renewcommand{\baselinestretch}{1.2} %larger linespace

\topmargin-1.3cm \headheight0cm \headsep1cm \topskip1cm \leftmargini0.5cm \textwidth17cm \textheight24.5cm \footskip1cm   \oddsidemargin-0.5cm \evensidemargin-0.5cm %adjust page size

\newcommand{\tabnewline}{\newline}

%+++++++++++++++++++++++++++++++++++++++
\begin{document}
\pagenumbering{roman} 
\setcounter{page}{0}
\pagestyle{empty}

\headsep3cm 
\begin{center}
%{\Large {\it Python based Multibody System Dynamics Simulation}}\vspace{1cm}\\
%{\Large {\it fleXible multibody system DYNAmics Parallelized with Python interface}}\vspace{1cm}\\
{\Large {\it Flexible Multibody Dynamics Systems with Python and C++}}\vspace{1cm}\\
{\Huge {\bf \codeName}} \vspace{0.5cm}\\
%{\small (flEXible mUltibody DYNamics )} \vspace{1cm}\\
{\small (flexible multibody dynamics )} \vspace{1cm}\\
{\Large \it User documentation} \vspace{1cm}\\
%\vspace{6cm}
\vspace{1cm}
\includegraphics[height=12cm]{intro1.png}\\
\vspace{0.5cm}
{\tiny \input{version}}\\
\vspace{1.5cm}
University of Innsbruck, Department of Mechatronics, \today,\vspace{0.25cm}\\
Johannes Gerstmayr\vspace{2cm}
\end{center}

\newpage

\pagestyle{plain}
\headsep0.7cm 
%\headsep1cm 
%\chapter*{Contents}
%
\tableofcontents


%%++++++++++++++++++++++++++++++++++++++++++++++
\clearpage
\pagenumbering{arabic} 
\setcounter{page}{0}

%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
\mysection{Introduction and Tutorial}
The documentation for \codeName\ is split into this introductory section, including a quick start up, code structure and important hints, 
as well as a couple of sections containing references to the available Python interfaces to interact with \codeName\ and finally some information on theory (e.g., 'Solver').

\mysubsection{Getting started}
This section will show:
\bn
	\item What is \codeName ?
	\item Goals of \codeName
	\item Who is developing \codeName ?
	\item How to install \codeName\ 
	\item How to link \codeName\ and Python
	\item Run a simple example in Spyder
\en
%
\mysubsubsection{What is \codeName}
\codeName\ is a C++ based Python library for efficient simulation of flexible multibody dynamics systems.
It is designed to easily set up complex multibody models, consisting of rigid and flexible bodies with joints, loads and other components.
\vspace{6pt}\\
%
\mysubsubsection{Goals of \codeName}
After the first development phase (Q4 2021), it will
\bi
  \item be a small multibody library, which can be easily linked to other projects,
	\item allow to efficiently simulate small scale systems (compute 100000s time steps per second for systems with $n_{DOF}<10$),
	\item allow to efficiently simulate medium scaled systems for problems with $n_{DOF} < 1\,000\,000$ (planned: Q4 2020),
	\item use multi-threaded parallel computing techniques (planned: Q4 2020),
	\item be accessible safe and at a wide range via the Python interface,
	\item allow to add user defined objects in C++,
	\item allow to add user defined objects in Python (planned: 2021),
	\item allow to add user defined solvers in Python (planned:Q2 2020).
\ei
%
\mysubsubsection{Who is developing \codeName ?}
\codeName\ is currently (Nov 2019) developed at the University of Innsbruck.
In the first phase most of the core code has been (and still is) written by Johannes Gerstmayr, implementing ideas of earlier developments of HOTINT. 15 years of development led to a lot of lessions learned.

Some specific codes regarding pybind interface and parallelization have been written by Stefan Holzinger, who also supports the upload to GitLab.
Important discussions with researchers from the community where important for the design and development of \codeName , where we like to mention Joachim Sch{\"o}berl from TU-Vienna who influenced the design of the code. During a Comet-K2 cooperation project, several discussions with the TMECH/LCM group in Linz influenced the code development.

The cooperation within the EU H2020-MSCA-ITN project 'Joint Training on Numerical Modelling of Highly Flexible Structures for Industrial Applications' will support further development of the code.
%
\mysubsubsection{How to install \codeName}
\codeName\ is still under intensive development of core modules.
There are several ways to using it, but you cannot install \codeName\ as compared to other programs and apps.
\vspace{6pt}\\
{\bf Ways to use \codeName }:
\bi
	\item Windows / Microsoft Visual Studio 17 and above:
	\bi
		\item get the files from git
		\item put them into a local directory (recommended: \texttt{C:\\DATA\textbackslash\textbackslash cpp\textbackslash\textbackslash EXUDYN\_git})
		\item start \texttt{main\_sln.sln} with Visual Studio
		\item compile the code and run \texttt{main\textbackslash\textbackslash pythonDev\textbackslash\textbackslash pytest.py} example code
		\item adapt \texttt{pytest.py} for your applications
		\item extend the C++ source code
		\item link it to your own code
	\ei
	\item Linux, etc.: not fully supported yet; however, all libraries are Linux-compatible and thus should run with minimum adaptation efforts
\ei

\mysubsubsection{How to link \codeName\ and Python}

In order to run \codeName , you need an appropriate Python installation.
We recommend to use
\bi
  \item Anaconda, 32bit, Python 3.6.5
	\item Spyder 3.2.8 (Python 3.6.5 32 bits)	
\ei
If you plan to use 64bit and newer Python versions, we recommend to use VS2019 to compile your code, which offers Python 3.7 compatibility.
However, you should know that Python versions and the version of the module must be identical (e.g., Python 3.6 32 {\bf both} in the exudyn module and in Spyder).

The simplest way is, to copy the files
\bi
  \item \texttt{exudynUtilities.py}
  \item \texttt{itemInterface.py}
  \item \texttt{exudyn.pyd}
\ei
to your working directory and directly import the modules as descriped in tutorials and examples.
Another way is to use Python's \texttt{sys} module to link to your \texttt{WorkingRelease} directory, for example:\vspace{6pt}\\
\pythonstyle
\begin{lstlisting}[language=Python, firstnumber=1]
	import sys
	sys.path.append('C:\\DATA\\cpp\\EXUDYN_git\\main\\bin\\WorkingRelease')
\end{lstlisting}

\mysubsubsection{Run a simple example in Spyder}
After performing the steps of the previous section, this section shows a simplistic model which helps you to check if \codeName\ runs on your computer.

In order to start, run the python interpreter Spyder.
For the following example, either 
\bi
	\item open Spyder and copy the example provided in Listing \ref{lst:firstexample} into a new file, or
	\item open \texttt{myFirstExample.py} from your \texttt{WorkingRelease} directory
\ei
Hereafter, press the play button or \texttt{F5} in Spyder.
%\lstinputlisting[language=Python]{../../main/bin/WorkingRelease/myFirstExample.py}
\pythonexternal[language=Python, frame=single, float, label=lst:firstexample, caption=My first example]{../../main/bin/WorkingRelease/myFirstExample.py}

If successful, the IPython Console of Spyder will print something like:
\plainlststyle
{\ttfamily \footnotesize
\begin{lstlisting}
  runfile('C:/DATA/cpp/EXUDYN_git/main/bin/WorkingRelease/myFirstExample.py', 
	  wdir='C:/DATA/cpp/EXUDYN_git/main/bin/WorkingRelease')
  Assemble nodes:
  Set initial system coordinates (for ODE2, ODE1 and Data coordinates) ...
  Time integration finished after 0.0011442 seconds.
\end{lstlisting}
}

If you check your current directory (where \texttt{myFirstExample.py} lies), you will find a new file \texttt{coordinatesSolution.txt}, which contains the results of your computation (with default values for time integration).
The beginning and end of the file should look like: \vspace{6pt}\\
{\ttfamily \footnotesize
\begin{lstlisting}[breaklines=true]
  #Exudyn generalized alpha solver solution file
  #simulation started=2019-11-14,20:35:12
  #columns contain: time, ODE2 displacements, ODE2 velocities, ODE2 accelerations, AE coordinates, ODE2 velocities
  #number of system coordinates [nODE2, nODE1, nAlgebraic, nData] = [2,0,0,0]
  #number of written coordinates [nODE2, nVel2, nAcc2, nODE1, nVel1, nAlgebraic, nData] = [2,2,2,0,0,0,0]
  #total columns exported  (excl. time) = 6
  #number of time steps (planned) = 100
  #
  0,0,0,0,0,0.0001,0
  0.02,2e-08,0,2e-06,0,0.0001,0
  0.03,4.5e-08,0,3e-06,0,0.0001,0
  0.04,8e-08,0,4e-06,0,0.0001,0
  0.05,1.25e-07,0,5e-06,0,0.0001,0

  ...

  0.96,4.608e-05,0,9.6e-05,0,0.0001,0
  0.97,4.7045e-05,0,9.7e-05,0,0.0001,0
  0.98,4.802e-05,0,9.8e-05,0,0.0001,0
  0.99,4.9005e-05,0,9.9e-05,0,0.0001,0
  1,5e-05,0,0.0001,0,0.0001,0
  #simulation finished=2019-11-14,20:35:12
  #Solver Info: errorOccurred=0,converged=1,solutionDiverged=0,total time steps=100,total Newton iterations=100,total Newton jacobians=100
\end{lstlisting}
}

Within this file, the first column shows the simulation time and the following columns provide solution of coordinates, their derivatives and Lagrange multipliers on system level.

\mysubsection{Module structure} \label{sec_programStructure}
This section will show:
\bi
  \item Overview of modules
  \item Conventions: dimension of nodes, objects and vectors
	\item Coordinates: reference coordinates and displacements
	\item Nodes, Objects, Markers and Loads
	\item Graphics pipeline
	\item Settings
	\item Generating output / results
	\item Interaction with the \codeName\ module (where do objects live? state machine; graphics interaction)
\ei

\mysubsubsection{Conventions: items, indices, coordinates}
In this documentation, we will use the term {\bf item} to identify nodes, objects, markers and loads:
\be
  \mathrm{item} \in \{\mathrm{node}, \mathrm{object}, \mathrm{marker}, \mathrm{load} \}
\ee
\vspace{12pt}\\
{\bf Indices: arrays and vector starting with 0:} \vspace{6pt}\\
As known from Python, all {\bf indices} of arrays, vectors, etc.\ are starting with 0. This means that the first component of the vector \texttt{v=[1,2,3]} is accessed with \texttt{v[0]} in Python (and also in the C++ part of \codeName ). The range is usually defined as \texttt{range(0,3)}, in which '3' marks the index after the last valid component of an array or vector.
%
\vspace{12pt}\\
{\bf Dimensionality of objects and vectors:}\vspace{6pt}\\ 
As a convention, quantities in \codeName\ are 3D, such as nodes, objects, markers, loads, measured quantities, etc. 
For that reason, we denote planar nodes, objects, etc.\ with the suffix '2D', but 3D objects do not get this suffix.

Output and input to objects, markers, loads, etc.\ is usually given by 3D vectors (or matrices), such as (local) position, force, torque, rotation, etc. However, initial and reference values for nodes depend on their dimensionality.
As an example, consider a \texttt{NodePoint2D}:
\bi
  \item \texttt{referenceCoordinates} is a 2D vector (but could be any dimension in general nodes)
	\item measuring the current position of \texttt{NodePoint2D} gives a 3D vector
	\item when attaching a \texttt{MarkerNodePosition} and a \texttt{LoadForceVector}, the force will be still a 3D vector
\ei
Furthermore, the local position in 2D objects is provided by a 3D vector. Usually, the dimensionality is given in the reference manual. User errors in the dimensionality will be usually detected either by the python interface (i.e., at the time the item is created) or by the system-preprocessor

\mysubsubsection{Nodes}
Nodes provide the coordinates (and the degrees of freedom) to the system. They have no mass, stiffness or whatsoever assigned.
Without nodes, the system has no unknown coordinates.
Adding a node provides (for the system unknown) coordinates. In addition we also need equations for every nodal coordinate -- otherwise the system cannot be computed (NOTE: this is currently not checked by the preprocessor).

\mysubsubsection{Objects}
Objects are 'computational objects' and they provide equations to your system. Objects additionally often provide derivatives and have measurable quantities (e.g. displacement) and they provide access, which can be used to apply, e.g., forces.

Objects can be a:
\bi
  \item general object (e.g.\ a controller, user defined object, ...; no example yet)
	\item body: has a mass or mass distribution; markers can be placed on bodies; loads can be applied; constraints can be attached via markers; bodies can be:
	\bi
	  \item[--] ground object: has no nodes
	  \item[--] simple body: has one node (e.g. mass point, rigid body)
	  \item[--] finite element and more complicated body (e.g. FFRF-object): has more than one node
	\ei
	\item connector: uses markers to connect nodes and/or bodies; adds additional terms to system equations either based on stiffness/damping or with constraints (and Lagrange multipliers). Possible connectors:
	\bi
		\item[--] algebraic constraint (e.g. constrain two coordinates: $q_1 = q_2$)
		\item[--] classical joint
		\item[--] spring-damper or penalty constraint
	\ei
\ei

\mysubsubsection{Markers}
Markers are interfaces between objects/nodes and constraints/loads.
A constraint (which is also an object) or load cannot act directly on a node or object without a marker.
As a benefit, the constraint or load does not need to know whether it is applied, e.g., to a node or to a local position of a body.

Typical situations are:
\bi
  \item Node -- Marker -- Load
	\item Node -- Marker -- Constraint (object)
	\item Body(object) -- Marker -- Load
	\item Body1 -- Marker1 -- Joint(object) -- Marker2 -- Body2
\ei

\mysubsubsection{Loads}
Loads are used to apply forces and torques to the system. The load values are static values. However, you can use Python functionality to modify loads either by linearly increasing them during static computation or by using the 'preStepPyExecute' structure in order to modify loads in every integration step depending on time or on measured quantities (thus, creating a controller).

\mysubsubsection{Reference coordinates and displacements}
Nodes usually have separated reference and initial quantities. Here, 
\texttt{referenceCoordinates} are the coordinates for which the system is defined upon creation. Reference coordinates are needed, e.g., for definition of joints and for the reference configuration of finite elements. In many cases it marks the undeformed configuration (e.g., with finite elements), but not, e.g., for \texttt{ObjectConnectorSpringDamper}, which has its own reference length. 

Initial displacement (or rotation) values are provided separately, in order to start a system from a configuration different from the reference configuration.
As an example, the initial configuration of a \texttt{NodePoint} is given by \texttt{referenceCoordinates + initialDisplacements}, while the initial state of a dynamic system additionally needs \texttt{initialVelocities}.


\mysubsubsection{Interaction with the \codeName\ module}
It is important that the \codeName\ module is basically a state machine, where you create items on the C++ side using the Python interface. This helps you to easily set up models using many other Python modules (numpy, sympy, matplotlib, ...) while the computation will be performed in the end on the C++ side in a very efficient manner. 

Commands such as \texttt{mbs.AddNode(...)} add objects to the system \texttt{mbs}. 
The system will be prepared for simulation by \texttt{mbs.Assemble()} and can be solved and evaluated hereafter.
Using \texttt{mbs.Reset()} will clear the system and allows to set up a new system. Items can be modified (\texttt{ModifyObject(...)}) after first initialization, even during simulation.
%
\vspace{24pt}\\
%
These sections will be extended soon!

%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

\mysubsection{Tutorial}

This section shall show:
\bi
  \item A basic tutorial for a 1D mass and spring-damper with initial displacements, shortest possible model with practically no special settings
	\item A more advanced 2D rigid-body model ({\it coming soon})
	\item Links to examples section
\ei
%
The tutorial of this section can be found in file:
\bi
	\item[] \texttt{main/pythonDev/Examples/springDamperTutorial.py}
\ei
This tutorial will set up a mass point and a spring damper, dynamically compute the solution and evaluate the reference solution.
\vspace{6pt}\\
To start up, we set the system path to the directory of the library (needs to be adjusted!):
\pythonstyle
\begin{lstlisting}[language=Python, firstnumber=1]
  import sys 
  sys.path.append('C:\\DATA\\cpp\\EXUDYN_git\\main\\bin\\WorkingRelease') #for Spider
\end{lstlisting}
%
We import the exudyn library and the interface for all nodes, objects, markers and loads:
\begin{lstlisting}[language=Python, firstnumber=3]
	import exudyn as exu
	from itemInterface import *
	import numpy as np
\end{lstlisting}
%
Next, we need a SystemContainer, which contains all computable systems and add a new system.
Per default, you always should name your system 'mbs' (multibody system), in order to copy/paste code parts from other examples, tutorials and other projects:
\begin{lstlisting}[language=Python, firstnumber=6]
	SC = exu.SystemContainer()
	mbs = SC.AddSystem()
\end{lstlisting}
%
In order to check, which version you are using, you can printout the current \codeName\ version. This version is in line with the issue tracker and marks the number of open/closed issues added to \codeName :
\begin{lstlisting}[language=Python, firstnumber=8]
	print('EXUDYN version='+exu.__version__)
\end{lstlisting}
%
Using the powerful Python language, we can define some variables for our problem, which will also be used for the analytical solution:
\begin{lstlisting}[language=Python, firstnumber=9]
	L=0.5               #reference position of mass
	mass = 1.6          #mass in kg
	spring = 4000       #stiffness of spring-damper in N/m
	damper = 8          #damping constant in N/(m/s)
	f =80               #force on mass
\end{lstlisting}
%
For the simple spring-mass-damper system, we need initial displacements and velocities:
\begin{lstlisting}[language=Python, firstnumber=14]
	u0=-0.08            #initial displacement
	v0=1                #initial velocity
	x0=f/spring         #static displacement
	print('resonance frequency = '+str(np.sqrt(spring/mass)))
	print('static displacement = '+str(x0))
\end{lstlisting}
%
We first need to add nodes, which provide the coordinates (and the degrees of freedom) to the system.
The following line adds a 3D node for 3D mass point\footnote{Note: Point is an abbreviation for NodePoint, defined in \texttt{itemInterface.py}.}:
\begin{lstlisting}[language=Python, firstnumber=19]
	n1=mbs.AddNode(Point(referenceCoordinates = [L,0,0], 
                       initialDisplacements = [u0,0,0], 
                       initialVelocities = [v0,0,0]))
\end{lstlisting}
Here, \texttt{Point} (=\texttt{NodePoint}) is a Python class, which takes a number of arguments defined in the reference manual. The arguments here are \texttt{referenceCoordinates}, which are the coordinates for which the system is defined. The initial configuration is given by \texttt{referenceCoordinates + initialDisplacements}, while the initial state additionally gets \texttt{initialVelocities}.

%
While \texttt{Point} adds 3 unknown coordinates to the system, which need to be solved, we also can add ground nodes, which can be used similar to nodes, but they do not have unknown coordinates -- and therefore also have no initial displacements or velocities. The advantage of ground nodes (and ground bodies) is that no constraints are needed to fix these nodes.
Such a ground node is added via:
\begin{lstlisting}[language=Python, firstnumber=22]
	nGround=mbs.AddNode(NodePointGround(referenceCoordinates = [0,0,0]))
\end{lstlisting}
%
In the next step, we add an object\footnote{For the moment, we just need to know that objects either depend on one or more nodes, which are usually bodies and finite elements, or they can be connectors, which connect (the coordinates of) objects via markers, see Section \ref{sec_programStructure}.}, which provides equations for coordinates. The \texttt{MassPoint} needs at least a mass (kg) and a node number to which the mass point is attached. Additionally, graphical objects could be attached:
\begin{lstlisting}[language=Python, firstnumber=23]
	massPoint = mbs.AddObject(MassPoint(physicsMass = mass, nodeNumber = n1))
\end{lstlisting}
%
In order to apply constraints and loads, we need markers. These markers are used as local positions (and frames), where we can attach a constraint lateron. In this example, we work on the coordinate level, both for forces as well as for constraints.
Markers are attached to the according ground and regular node number, additionally using a coordinate number (0 ... first coordinate):
\begin{lstlisting}[language=Python, firstnumber=24]
	groundMarker=mbs.AddMarker(MarkerNodeCoordinate(nodeNumber= nGround, 
	                                                coordinate = 0))
	#marker for springDamper for first (x-)coordinate:
	nodeMarker  =mbs.AddMarker(MarkerNodeCoordinate(nodeNumber= n1, 
	                                                coordinate = 0))
\end{lstlisting}
This means that loads can be applied to the first coordinate of node \texttt{n1} via marker with number \texttt{nodeMarker}.

Now we add a spring-damper to the markers with numbers \texttt{groundMarker} and the \texttt{nodeMarker}, providing stiffness and damping parameters:
\begin{lstlisting}[language=Python, firstnumber=29]
	mbs.AddObject(CoordinateSpringDamper(markerNumbers = [groundMarker, nodeMarker], 
	                                     stiffness = spring, 
	                                     damping = damper)) 
\end{lstlisting}
%
Finally, a load is added to marker \texttt{nodeMarker}, with a scalar load with value \texttt{f}:
\begin{lstlisting}[language=Python, firstnumber=32]
	mbs.AddLoad(LoadCoordinate(markerNumber = nodeMarker, 
	                           load = f))
\end{lstlisting}
%
As our system is fully set, we can print the overall information and assemble the system to make it ready for simulation:
\begin{lstlisting}[language=Python, firstnumber=34]
	print(mbs)
	mbs.Assemble()
\end{lstlisting}
%
We will use time integration and therefore define a number of steps (fixed step size; must be provided) and the total time span for the simulation:
\begin{lstlisting}[language=Python, firstnumber=36]
	steps = 1000  #number of steps to show solution
	tEnd = 1     #end time of simulation
\end{lstlisting}
%
All settings for simulation, see according reference section, can be provided in a structure given from \texttt{exu.SimulationSettings()}. Note that this structure will contain all default values, and only non-default values need to be provided:
\begin{lstlisting}[language=Python, firstnumber=38]
	simulationSettings = exu.SimulationSettings()
	simulationSettings.solutionSettings.solutionWritePeriod = 1e-2  #output interval
	simulationSettings.timeIntegration.numberOfSteps = steps
	simulationSettings.timeIntegration.endTime = tEnd
\end{lstlisting}
%
We are using a generalized alpha solver, where numerical damping is needed for index 3 constraints. As we have only spring-dampers, we can set the spectral radius to 1, meaning no numerical damping:
\begin{lstlisting}[language=Python, firstnumber=42]
	simulationSettings.timeIntegration.generalizedAlpha.spectralRadius = 1
\end{lstlisting}
%
In order to visualize the results online, a renderer can be started. As our computation will be very fast, it is a good idea to wait for the user to press SPACE, before starting the simulation (uncomment second line):
\begin{lstlisting}[language=Python, firstnumber=43]
	exu.StartRenderer()              #start graphics visualization
	#mbs.WaitForUserToContinue()    #wait for pressing SPACE bar to continue
\end{lstlisting}
As the simulation is still very fast, we will not see the motion of our node. Using e.g.\ \texttt{steps=10000000} in the lines above allows you online visualize the resulting oscillations.

%
Finally, we start the solver, by telling which system to be solved, solver type and the simulation settings:
\begin{lstlisting}[language=Python, firstnumber=45]
	SC.TimeIntegrationSolve(mbs, 'GeneralizedAlpha', simulationSettings)
\end{lstlisting}
%

After simulation, our renderer needs to be stopped (otherwise it would stay in background and prohibit further simulations). 
Sometimes you would like to wait until closing the render window, using \texttt{WaitForRenderEngineStopFlag()}:
\begin{lstlisting}[language=Python, firstnumber=46]
	#SC.WaitForRenderEngineStopFlag()#wait for pressing 'Q' to quit
	exu.StopRenderer()               #safely close rendering window!
\end{lstlisting}
%
There are several ways to evaluate results, see the reference pages. In the following we take the final value of node \texttt{n1} and read its 3D position vector:
\begin{lstlisting}[language=Python, firstnumber=48]
	#evaluate final (=current) output values
	u = mbs.GetNodeOutput(n1, exu.OutputVariableType.Position)
	print('displacement=',u)
\end{lstlisting}
%
The following code generates a reference (exact) solution for our example:
\begin{lstlisting}[language=Python, firstnumber=51]
	import matplotlib.pyplot as plt
	import matplotlib.ticker as ticker

	omega0 = np.sqrt(spring/mass)  #eigen frequency of undamped system
	dRel = damper/(2*np.sqrt(spring*mass)) #dimensionless damping
	omega = omega0*np.sqrt(1-dRel**2) #eigen freq of damped system
	C1 = u0-x0 #static solution needs to be considered!
	C2 = (v0+omega0*dRel*C1) / omega #C1, C2 are coeffs for solution

	refSol = np.zeros((steps+1,2))
	for i in range(0,steps+1):
	  t = tEnd*i/steps
	  refSol[i,0] = t
	  refSol[i,1] = np.exp(-omega0*dRel*t)*(C1*np.cos(omega*t)+C2*np.sin(omega*t))+x0

	plt.plot(refSol[:,0], refSol[:,1], 'r-') #exact solution
\end{lstlisting}
%
Now we can load our results from the default solution file \texttt{coordinatesSolution.txt}, which is in the same
directory as your python tutorial file. For convenient reading the file containing commented lines, we use a numpy feature and
finally plot the displacement of coordinate 0 or our mass point\footnote{\texttt{data[:,0]} contains the simulation time, \texttt{data[:,1]} contains displacement of (global) coordinate 0, \texttt{data[:,2]} contains displacement of (global) coordinate 1, ...)}:
\begin{lstlisting}[language=Python, firstnumber=70]
	data = np.loadtxt('coordinatesSolution.txt', comments='#', delimiter=',')
	plt.plot(data[:,0], data[:,1], 'b-') #numerical solution
\end{lstlisting}
%
In order to get a nice plot within Spyder, the following options can be used\footnote{note, in some environments you need finally the command \texttt{plt.show()}}:
\begin{lstlisting}[language=Python, firstnumber=72]
	ax=plt.gca() # get current axes
	ax.grid(True, 'major', 'both')
	ax.xaxis.set_major_locator(ticker.MaxNLocator(10))
	ax.yaxis.set_major_locator(ticker.MaxNLocator(10))
	plt.tight_layout()
	plt.show() 
\end{lstlisting}
%
%
\vspace{24pt}
Further examples can be found in your copy of exudyn: 
\bi
	\item[] \texttt{main/pythonDev/Examples}
\ei





%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

\mysection{Python-C++ command interface}
This section lists the basic interface functions which can be used to set up a \codeName\ model in Python.

To import the module, just include the \codeName\ module in Python (for compatibility with examples and other users, we recommend to use the 'exu' abbreviation throughout):
\bi
  \item[] \texttt{import exudyn as exu}
\ei
The exudyn module will usually hold one SystemContainer, which is a class that is initialized by assigning a system container to a variable, usually denoted as 'SC':
\bi
  \item[] \texttt{SC = exu.SystemContainer()}
\ei
Furthermore, there are a couple of commands available directly in the \codeName\ module, given in the following subsections.
Regarding the {\bf (basic) module access}, functions are related to the '\texttt{exudyn = exu}' module, see these examples:
\begin{lstlisting}[language=Python, firstnumber=14]
  import exudyn as exu
  SC = exu.SystemContainer()
  exu.InfoStat() 
  exu.Go()
  nInvalid = exu.InvalidIndex()
\end{lstlisting} \vspace{12pt}
%
Understanding the usage of functions for python object '\texttt{SystemContainer}' provided by \codeName, the following examples might help:
\begin{lstlisting}[language=Python, firstnumber=14]
  import exudyn as exu
  SC = exu.SystemContainer()
  mbs = SC.AddSystem()
  nSys = SC.NumberOfSystems()
  print(nSys)
  SC.Reset()
\end{lstlisting} \vspace{12pt}
%
Understanding the usage of functions for the '\texttt{MainSystem}' provided by \texttt{SystemContainer}, the following examples might help:
\begin{lstlisting}[language=Python, firstnumber=14]
  import exudyn as exu
  SC = exu.SystemContainer()
  mbs = SC.AddSystem()
  mbs.Reset()
  mbs.WaitForUserToContinue() 
\end{lstlisting} \vspace{12pt}
%

\input{manual_interfaces}


\mysection{Objects, nodes, markers and loads reference manual}
This section includes the reference manual for all objects (bodies/constraints), nodes, markers and loads.

\input{itemDefinition}

\mysubsection{GraphicsData}
Some items may include a 'graphicsData' structure. GraphicsData contains a list of graphicsData items, i.e.\ graphicsData = [graphicsItem1, graphicsItem2, ...]. Every single graphicsItem may be defined as one of the following structures using a specific 'type':
\begin{center}
  \footnotesize
  \begin{longtable}{| p{3cm} | p{2cm} | p{3cm} | p{7.5cm} |} 
	\hline
  \bf Name & \bf type & \bf default value & \bf description \\ \hline
%
%LINE
	\multicolumn{3}{l}{\parbox{8cm}{\bf type = 'Line': }} & \multicolumn{1}{l}{\parbox{7.5cm}{\it draws a polygonal line between all specified points}}\\ \hline
  color & list & [0,0,0,1] & list of 4 floats to define RGB-color and transparency\\ \hline
  data & list &  mandatory & list of float triples of x,y,z coordinates of the line floats to define RGB-color and transparency; Example: data=[0,0,0, 1,0,0, 1,1,0, 0,1,0, 0,0,0] ... draws a rectangle with side length 1\\ \hline
%
%CIRCLE
	\multicolumn{3}{l}{\parbox{8cm}{\bf type = 'Circle': }} & \multicolumn{1}{l}{\parbox{7.5cm}{\it draws a circle with center point, normal (defines plane of circle) and radius}}\\ \hline
  color & list & [0,0,0,1] & list of 4 floats to define RGB-color and transparency\\ \hline
  radius & float & mandatory & radius\\ \hline
  position & list & mandatory & list of float triples of x,y,z coordinates of center point of the circle\\ \hline
  normal & list & [0,0,1] & list of float triples of x,y,z coordinates of normal to the plane of the circle; the default value gives a circle in the ($x,y$)-plane\\ \hline
%TEXT
	\multicolumn{3}{l}{\parbox{8cm}{\bf type = 'Text': }} & \multicolumn{1}{l}{\parbox{7.5cm}{\it places the given text at position}}\\ \hline
  color & list & [0,0,0,1] & list of 4 floats to define RGB-color and transparency\\ \hline
  text & string & mandatory & text to be displayed\\ \hline
  position & list & mandatory & list of float triples of [x,y,z] coordinates of the left upper position of the text; e.g.\ position=[20,10,0] \\ \hline
%TRIANGLELIST
	\multicolumn{3}{l}{\parbox{8cm}{\bf type = 'TriangleList': }} & \multicolumn{1}{l}{\parbox{7.5cm}{\it draws a flat triangle mesh for given points and connectivity}}\\ \hline
  points & list & mandatory & list [x0,y0,z0, x1,y1,z1, ...] containing $n \times 3$ floats (grouped x0,y0,z0, x1,y1,z1, ...) to define x,y,z coordinates of points, $n$ being the number of points (=vertices)\\ \hline
  colors & list & empty & list [R0,G0,B0,A0, R1,G2,B1,A1, ...] containing $n \times 4$ floats to define RGB-color and transparency A, where $n$ must be according to number of points; if field 'colors' does not exist, default colors will be used\\ \hline
  normals & list & empty & list [n0x,n0y,n0z, ...] containing $n \times 3$ floats to define normal direction of triangles per point, where $n$ must be according to number of points; if field 'normals' does not exist, default normals [0,0,0] will be used\\ \hline
  triangles & list &  mandatory & list [T0point0, T0point1, T0point2, ...] containing $n_{trig} \times 3$ floats to define point indices of each vertex of the triangles (=connectivity); point indices start with index 0; the maximum index must be $\le$ points.size()\\ \hline
\end{longtable}
%
Examples of \texttt{GraphicsData} can be found in the Python examples and in \texttt{exudynUtilities.py}.
%
\end{center}


\mysection{\codeName\ Settings: simulation and visualization}
This section includes the reference manual for settings which are available in the python interface, e.g.\ simulation settings, visualization settings, and others.

\input{interfaces}

\mysection{3D Graphics Visualization}

The 3D graphics visualization window is kept simple, but useful to see the animated results of the multibody system.

\mysubsection{Mouse input}
The following table includes the mouse functions. 

\begin{center}
  \footnotesize
  \begin{longtable}{| p{4cm} | p{4cm} | p{8cm} |} 
	\hline
  \bf Button & action & \bf remarks \\ \hline
  \bf left mouse button & move model & keep left mouse button pressed to move the model in the current x/y plane\\ \hline
  \bf right mouse button & rotate model & keep right mouse button pressed to rotate model around current current $X_1$/$X_2$ axes\\ \hline
  \bf mouse wheel & zoom & use mouse wheel to zoom (on touch screens 'pinch-to-zoom' might work as well) \\ \hline
  \end{longtable}
\end{center}


\mysubsection{Keyboard input}
The following table includes the keyboard shortcuts available in the window. 

\begin{center}
  \footnotesize
  \begin{longtable}{| p{4cm} | p{4cm} | p{8cm} |} 
	\hline
  \bf Key(s) & action & \bf remarks \\ \hline
  \bf 1,2,3,4 or 5 & visualization update & the entered digit controls the visualization update, which can be changed from 1=1 update per 20ms to 5=1 update per 100s\\ \hline
  \bf '.' or KEYPAD '+' & zoom in & zoom one step into scene \\ \hline
  \bf ',' or KEYPAD '-' & zoom out & zoom one step out of scene \\ \hline
	%
  \bf 0 or KEYPAD '0'& reset rotation & set rotation such that the scene is oriented in the x/y plane \\ \hline
  \bf A & zoom all & set zoom such that the whole scene is visible \\ \hline
  \bf CURSOR UP, DOWN, ... & move scene& use coursor keys to move the scene up, down, left, and right \\ \hline
	%
  \bf C & show/hide connectors & pressing this key switches the visibility of connectors \\ \hline
  \bf CTRL+C & show/hide connector numbers & pressing this key switches the visibility of connector numbers \\ \hline
  %
	\bf B & show/hide bodies & pressing this key switches the visibility of bodies \\ \hline
  \bf CTRL+B & show/hide body numbers & pressing this key switches the visibility of body numbers \\ \hline
  %
	\bf L & show/hide loads & pressing this key switches the visibility of loads \\ \hline
  \bf CTRL+L & show/hide load numbers & pressing this key switches the visibility of load numbers \\ \hline
  %
	\bf M & show/hide markers & pressing this key switches the visibility of markers \\ \hline
  \bf CTRL+M & show/hide marker numbers & pressing this key switches the visibility of marker numbers \\ \hline
  %
	\bf N & show/hide nodes & pressing this key switches the visibility of nodes \\ \hline
  \bf CTRL+N & show/hide node numbers & pressing this key switches the visibility of node numbers \\ \hline
  %
	\bf Q & stop simulation & simulation is stopped and cannot be recovered \\ \hline
  \bf ESCAPE & close render window & stops the simulation and closes the render window \\ \hline
  \bf SPACE & continue simulation & if simulation is paused, it can be continued by pressing space; use SHIFT+SPACE to continuously activate 'continue simulation'\\ \hline
  \end{longtable}
\end{center}


\mysection{Solver}
\input{solver}



\clearpage

\mysection{References} 
coming soon!

\mysection{License} 

%\verbatiminput{././LICENSE.txt}
%\input{../../licenseEXUDYN.txt}
%\lstinputlisting{..\..\LICENSE.txt}
\plainlststyle
\lstinputlisting[breaklines=true, basicstyle=\ttm]{../../LICENSE.txt}
\end{document}


