% definition of structures
%+++++++++++++++++++++++++++++++++++
\mysubsection{SolutionSettings}
General settings for exporting the solution (results) of a simulation.\\ 
%
SolutionSettings has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    writeSolutionToFile &     bool &      &     True &     flag (true/false), which determines if (global) solution vector is written to file\\ \hline
    appendToFile &     bool &      &     False &     flag (true/false); if true, solution is always appended to file\\ \hline
    writeFileHeader &     bool &      &     True &     flag (true/false); if true, file header is written (turn off, e.g. for multiple runs of time integration)\\ \hline
    writeFileFooter &     bool &      &     True &     flag (true/false); if true, information at end of simulation is written: convergence, total solution time, statistics\\ \hline
    solutionWritePeriod &     UReal &      &     0.01 &     time span (period), determines how often the solution is written during a (dynamic) simulation\\ \hline
    exportVelocities &     bool &      &     True &     solution is written as displacements, velocities[, accelerations] [,algebraicCoordinates] [,DataCoordinates]\\ \hline
    exportAccelerations &     bool &      &     True &     solution is written as displacements, [velocities,] accelerations [,algebraicCoordinates] [,DataCoordinates]\\ \hline
    exportAlgebraicCoordinates &     bool &      &     True &     solution is written as displacements, [velocities,] [accelerations,], algebraicCoordinates (=Lagrange multipliers) [,DataCoordinates]\\ \hline
    exportDataCoordinates &     bool &      &     True &     solution is written as displacements, [velocities,] [accelerations,] [,algebraicCoordinates (=Lagrange multipliers)] ,DataCoordinates\\ \hline
    coordinatesSolutionFileName &     String &      &     'coordinatesSolution.txt' &     filename and (relative) path of solution file containing all coordinates versus time\\ \hline
    solutionInformation &     String &      &     '' &     special information added to header of solution file (e.g. parameters and settings, modes, ...)\\ \hline
    outputPrecision &     Index &      &     10 &     precision for floating point numbers written to solution files\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{NumericalDifferentiation}
Settings for numerical differentiation of a function (needed for computation of numerical jacobian e.g. in implizit integration); HOTINT1: relativeEpsilon * Maximum(minimumCoordinateSize, fabs(x(i))).\\ 
%
NumericalDifferentiation has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    relativeEpsilon &     UReal &      &     1e-7 &     relative differentiation parameter epsilon; the numerical differentiation parameter $\varepsilon$ follows from the formula ($\varepsilon = \varepsilon_\mathrm{relative}*max(q_{min}, |q_i + [q^{Ref}_i]|)$, with $\varepsilon_\mathrm{relative}$=relativeEpsilon, $q_{min} = $minimumCoordinateSize, $q_i$ is the current coordinate which is differentiated, and $qRef_i$ is the reference coordinate of the current coordinate\\ \hline
    minimumCoordinateSize &     UReal &      &     1e-2 &     minimum size of coordinates in relative differentiation parameter\\ \hline
    doSystemWideDifferentiation &     bool &      &     False &     true: system wide differentiation (e.g. all ODE2 equations w.r.t. all ODE2 coordinates); false: only local (object) differentiation\\ \hline
    addReferenceCoordinatesToEpsilon &     bool &      &     False &     true: for the size estimation of the differentiation parameter, the reference coordinate $q^{Ref}_i$ is added to ODE2 coordinates --> see; false: only the current coordinate is used for size estimation of the differentiation parameter\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{Newton}
Settings for Newton method used in static or dynamic simulation.\\ 
%
Newton has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    numericalDifferentiation &     NumericalDifferentiation &      &      &     numerical differentiation parameters for numerical jacobian (e.g. Newton in static solver or implicit time integration)\\ \hline
    useNumericalDifferentiation &     bool &      &     False &     flag (true/false); false = perform direct computation of jacobian, true = use numerical differentiation for jacobian\\ \hline
    useNewtonSolver &     bool &      &     True &     flag (true/false); false = linear computation, true = use Newton solver for nonlinear solution\\ \hline
    relativeTolerance &     UReal &      &     1e-8 &     relative tolerance of residual for Newton (general goal of Newton is to decrease the residual by this factor)\\ \hline
    absoluteTolerance &     UReal &      &     1e-10 &     absolute tolerance of residual for Newton (needed e.g. if residual is fulfilled right at beginning); condition: sqrt(q*q)/numberOfCoordinates <= absoluteTolerance\\ \hline
    modifiedNewtonContractivity &     UReal &      &     0.5 &     maximum contractivity (=reduction of error in every Newton iteration) accepted by modified Newton; if contractivity is greater, a Jacobian update is computed\\ \hline
    useModifiedNewton &     Index &      &     False &     true: Jacobian only at first step; no Jacobian updates per step; false: Jacobian computed in every step\\ \hline
    maxIterations &     Index &      &     20 &     maximum number of iterations (including modified + restart Newton steps); after that iterations, the static/dynamic solver stops with error\\ \hline
    maxModifiedNewtonIterations &     Index &      &     10 &     maximum number of iterations for modified Newton (without Jacobian update); after that number of iterations, the modified Newton method gets a jacobian update and is further iterated\\ \hline
    maxModifiedNewtonRestartIterations &     Index &      &     5 &     maximum number of iterations for modified Newton after aJacobian update; after that number of iterations, the full Newton method is started for this step\\ \hline
    maximumSolutionNorm &     UReal &      &     1e38 &     this is the maximum allowed value for solutionU.L2NormSquared() which is the square of the square norm (value=$u_1^2$+$u_2^2$+...), and solutionV/A...; if the norm of solution vectors are larger, Newton method is stopped; the default value is chosen such that it would still work for single precision numbers (float)\\ \hline
    maxDiscontinuousIterations &     Index &      &     5 &     maximum number of discontinuous (post Newton) iterations\\ \hline
    discontinuousIterationTolerance &     UReal &      &     1 &     absolute tolerance for discontinuous (post Newton) iterations; the errors represent absolute residuals and can be quite high\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{GeneralizedAlpha}
Settings for generalized-alpha, implicit trapezoidal or Newmark time integration methods.\\ 
%
GeneralizedAlpha has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    newmarkBeta &     UReal &      &     0.25 &     value beta for Newmark method; default value beta = $\frac 1 4$ corresponds to (undamped) trapezoidal rule\\ \hline
    newmarkGamma &     UReal &      &     0.5 &     value gamma for Newmark method; default value gamma = $\frac 1 2$ corresponds to (undamped) trapezoidal rule\\ \hline
    useIndex2Constraints &     bool &      &     False &     set useIndex2Constraints = true in order to use index2 (velocity level constraints) formulation\\ \hline
    useNewmark &     bool &      &     False &     if true, use Newmark method with beta and gamma instead of generalized-Alpha\\ \hline
    spectralRadius &     UReal &      &     0.9 &     spectral radius for Generalized-alpha solver; set this value to 1 for no damping or to 0 < spectralRadius < 1 for damping of high-frequency dynamics; for position-level constraints (index 3), spectralRadius must be < 1\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{TimeIntegration}
General parameters used in time integration; specific parameters are provided in the according solver settings, e.g. for generalizedAlpha.\\ 
%
TimeIntegration has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    startTime &     UReal &      &     0 &     start time of time integration (usually set to zero)\\ \hline
    endTime &     UReal &      &     1 &     end time of time integration\\ \hline
    numberOfSteps &     UInt &      &     100 &     number of steps in time integration; stepsize is computed from (endTime-startTime)/numberOfSteps\\ \hline
    verboseMode &     Index &      &     0 &     0 ... no output, 1 ... show short step information every 2 seconds (error), 2 ... show every step information, 3 ... show also solution vector, 4 ... show also mass matrix and jacobian (implicit methods), 5 ... show also Jacobian inverse (implicit methods)\\ \hline
    pauseAfterEachStep &     bool &      &     False &     pause after every time step (user press SPACE)\\ \hline
    newton &     Newton &      &      &     parameters for Newton method; used for implicit time integration methods only\\ \hline
    generalizedAlpha &     GeneralizedAlpha &      &      &     parameters for generalized-alpha, implicit trapezoidal rule or Newmark (options only apply for these methods)\\ \hline
    preStepPyExecute &     String &      &     '' &     Python code to be executed prior to every step and after last step, e.g. for postprocessing\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{StaticSolver}
Settings for static solver linear or nonlinear (Newton).\\ 
%
StaticSolver has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    newton &     Newton &      &      &     parameters for Newton method (e.g. in static solver or time integration)\\ \hline
    numberOfLoadSteps &     Index &      &     1 &     number of load steps; if numberOfLoadSteps=1, no load steps are used and full forces are applied at once\\ \hline
    loadStepDuration &     UReal &      &     1 &     quasi-time for all load steps (added to current time in load steps)\\ \hline
    loadStepStart &     UReal &      &     0 &     a quasi time, which can be used for the output (first column) as well as for time-dependent forces; quasi-time is increased in every step i by loadStepDuration/numberOfLoadSteps; loadStepTime = loadStepStart + i*loadStepDuration/numberOfLoadSteps, but loadStepStart untouched ==> increment by user\\ \hline
    loadStepGeometric &     bool &      &     False &     if loadStepGeometric=false, the load steps are incremental (arithmetic series, e.g. 0.1,0.2,0.3,...); if true, the load steps are increased in a geometric series, e.g. for $n=8$ numberOfLoadSteps and $d = 1000$ loadStepGeometricRange, it follows: $1000^{1/8}/1000=0.00237$, $1000^{2/8}/1000=0.00562$, $1000^{3/8}/1000=0.0133$, ..., $1000^{7/8}/1000=0.422$, $1000^{8/8}/1000=1$\\ \hline
    loadStepGeometricRange &     UReal &      &     1000 &     if loadStepGeometric=true, the load steps are increased in a geometric series, see loadStepGeometric\\ \hline
    stabilizerODE2term &     UReal &      &     0 &     add mass-proportional stabilizer term in ODE2 part of jacobian for stabilization (scaled ), e.g. of badly conditioned problems; the diagnoal terms are scaled with $stabilizer = (1-loadStepFactor^2)$, and go to zero at the end of all load steps: $loadStepFactor=1$ -> $stabilizer = 0$\\ \hline
    verboseMode &     Index &      &     1 &     0 ... no output, 1 ... show errors and load steps, 2 ... show short Newton step information (error), 3 ... show also solution vector, 4 ... show also jacobian, 5 ... show also Jacobian inverse\\ \hline
    pauseAfterEachStep &     bool &      &     False &     pause after every load step (user press SPACE)\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{SimulationSettings}
General Settings for simulation; according settings for solution and solvers are given in subitems of this structure. \\ 
%
SimulationSettings has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    timeIntegration &     TimeIntegration &      &      &     time integration parameters\\ \hline
    solutionSettings &     SolutionSettings &      &      &     settings for solution files\\ \hline
    staticSolver &     StaticSolver &      &      &     static solver parameters\\ \hline
    linearSolverType &     LinearSolverType &      &     LinearSolverType::EXUdense &     selection of numerical linear solver: exu.EXUdense (dense matrix inverse), exu.EigenSparseLU (sparse matrix LU-factorization), ... (enumeration type)\\ \hline
    displayStatistics &     bool &      &     False &     display general computation information at end of time step (steps, iterations, function calls, step rejections, ...\\ \hline
    displayComputationTime &     bool &      &     True &     display computation time at end of time step\\ \hline
    outputPrecision &     Index &      &     6 &     precision for floating point numbers written to console; e.g. values written by solver\\ \hline
    numberOfThreads &     Index &      &     1 &     number of threads used for parallel computation (1 == scalar processing); not yet implemented (status: Nov 2019)\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{VSettingsGeneral}
General settings for visualization.\\ 
%
VSettingsGeneral has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    graphicsUpdateInterval &     float &      &     0.1 &     interval of graphics update during simulation in seconds; 0.1 = 10 frames per second; low numbers might slow down computation speed\\ \hline
    autoFitScene &     bool &      &     True &     automatically fit scene within first second after StartRenderer()\\ \hline
    textSize &     float &      &     12. &     general text size if not overwritten\\ \hline
    minSceneSize &     float &      &     0.1 &     minimum scene size for initial scene size and for autoFitScene, to avoid division by zero; SET GREATER THAN ZERO\\ \hline
    backgroundColor &     Float4 &     4 &     [1.,1.,1.,1.] &     red, green, blue and alpha values for background of render window (white=[1,1,1,1]; black = [0,0,0,1])\\ \hline
    coordinateSystemSize &     float &      &     0.4 &     size of coordinate system relative to screen\\ \hline
    drawCoordinateSystem &     bool &      &     True &     false = no coordinate system shown\\ \hline
    showComputationInfo &     bool &      &     True &     false = no info about computation (current time, solver, etc.) shown\\ \hline
    pointSize &     float &      &     0.01 &     global point size (absolute)\\ \hline
    circleTiling &     Index &      &     16 &     global number of segments for circles; if smaller than 2, 2 segments are used (flat)\\ \hline
    cylinderTiling &     Index &      &     16 &     global number of segments for cylinders; if smaller than 2, 2 segments are used (flat)\\ \hline
    sphereTiling &     Index &      &     8 &     global number of segments for spheres; if smaller than 2, 2 segments are used (flat)\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{VSettingsWindow}
Window and interaction settings for visualization; handle changes with care, as they might lead to unexpected results or crashes.\\ 
%
VSettingsWindow has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    renderWindowSize &     Index2 &     2 &     [1024,768] &     initial size of OpenGL render window in pixel\\ \hline
    startupTimeout &     Index &      &     5000 &     OpenGL render window startup timeout in ms (change might be necessary if CPU is very slow)\\ \hline
    keypressRotationStep &     float &      &     5. &     rotation increment per keypress in degree (full rotation = 360 degree)\\ \hline
    mouseMoveRotationFactor &     float &      &     1. &     rotation increment per 1 pixel mouse movement in degree\\ \hline
    keypressTranslationStep &     float &      &     0.1 &     translation increment per keypress relative to window size\\ \hline
    zoomStepFactor &     float &      &     1.15 &     change of zoom per keypress (keypad +/-) or mouse wheel increment\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{VSettingsOpenGL}
OpenGL settings for 2D and 2D rendering.\\ 
%
VSettingsOpenGL has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    initialCenterPoint &     Float3 &     3 &     [0.,0.,0.] &     centerpoint of scene (3D) at renderer startup; overwritten if autoFitScene = True\\ \hline
    initialZoom &     float &      &     1. &     initial zoom of scene; overwritten/ignored if autoFitScene = True\\ \hline
    initialMaxSceneSize &     float &      &     1. &     initial maximum scene size (auto: diagonal of cube with maximum scene coordinates); used for 'zoom all' functionality and for visibility of objects; overwritten if autoFitScene = True\\ \hline
    initialModelRotation &     Float9 &     9 &     [1.,0.,0., 0.,1.,0., 0.,0.,1.] &     initial model rotation matrix for OpenGl; floats are stored in raw-major format\\ \hline
    multiSampling &     Index &     1 &     1 &     multi sampling turned off (<=1) or turned on to given values (2, 4, 8 or 16); increases the graphics buffers and might crash due to graphics card memory limitations; only works if supported by hardware; if it does not work, try to change 3D graphics hardware settings!\\ \hline
    lineWidth &     float &     1 &     1. &     width of lines used for representation of lines, circles, points, etc.\\ \hline
    lineSmooth &     bool &     1 &     True &     draw lines smooth\\ \hline
    textLineWidth &     float &     1 &     1. &     width of lines used for representation of text\\ \hline
    textLineSmooth &     bool &     1 &     False &     draw lines for representation of text smooth\\ \hline
    showFaces &     bool &     1 &     True &     show faces of triangles, etc.; using the options showFaces=false and showFaceEdges=true gives are wire frame representation\\ \hline
    showFaceEdges &     bool &     1 &     False &     show edges of faces; using the options showFaces=false and showFaceEdges=true gives are wire frame representation\\ \hline
    shadeModelSmooth &     bool &     1 &     True &     true: turn on smoothing for shaders, which uses vertex normals to smooth surfaces\\ \hline
    materialSpecular &     Float4 &     4 &     [1.,1.,1.,1.] &     4f specular color of material\\ \hline
    materialShininess &     float &     1 &     60. &     shininess of material\\ \hline
    enableLight0 &     bool &     1 &     True &     turn on/off light0\\ \hline
    light0position &     Float4 &     4 &     [1.,1.,-1.,0.] &     4f position vector of GL light0; 4th value should be 0, otherwise the vector obtains a special interpretation, see opengl manuals\\ \hline
    light0ambient &     float &     1 &     0.25 &     ambient value of GL light0\\ \hline
    light0diffuse &     float &     1 &     0.4 &     diffuse value of GL light0\\ \hline
    light0specular &     float &     1 &     0.4 &     specular value of GL light0\\ \hline
    enableLight1 &     bool &     1 &     True &     turn on/off light1\\ \hline
    light1position &     Float4 &     4 &     [0.,3.,2.,0.] &     4f position vector of GL light1; 4th value should be 0, otherwise the vector obtains a special interpretation, see opengl manuals\\ \hline
    light1ambient &     float &     1 &     0.25 &     ambient value of GL light1\\ \hline
    light1diffuse &     float &     1 &     0.4 &     diffuse value of GL light1\\ \hline
    light1specular &     float &     1 &     0. &     specular value of GL light1\\ \hline
    drawFaceNormals &     bool &     1 &     False &     draws triangle normals, e.g. at center of triangles; used for debugging of faces\\ \hline
    drawVertexNormals &     bool &     1 &     False &     draws vertex normals; used for debugging\\ \hline
    drawNormalsLength &     float &     1 &     0.1 &     length of normals; used for debugging\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{VSettingsContour}
Settings for contour plots; use these options to visualize field data, such as displacements, stresses, strains, etc. for bodies, nodes and finite elements.\\ 
%
VSettingsContour has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    outputVariableComponent &     Index &     1 &     0 &     select the component of the chosen output variable; e.g., for displacements, 3 components are available: 0 == x, 1 == y, 2 == z component; if this component is not available by certain objects or nodes, no value is drawn\\ \hline
    outputVariable &     OutputVariableType &      &     OutputVariableType::None &     selected contour plot output variable type; select OutputVariableType.None to deactivate contour plotting.\\ \hline
    minValue &     float &     1 &     0 &     minimum value for contour plot; set manually, if automaticRange == False\\ \hline
    maxValue &     float &     1 &     1 &     maximum value for contour plot; set manually, if automaticRange == False\\ \hline
    automaticRange &     bool &      &     True &     if true, the contour plot value range is chosen automatically to the maximum range\\ \hline
    showColorBar &     bool &      &     True &     show the colour bar with minimum and maximum values for the contour plot\\ \hline
    colorBarTiling &     Index &     1 &     12 &     number of tiles (segements) shown in the colorbar for the contour plot\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{VSettingsNodes}
Visualization settings for nodes.\\ 
%
VSettingsNodes has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    show &     bool &      &     True &     flag to decide, whether the nodes are shown\\ \hline
    showNumbers &     bool &      &     False &     flag to decide, whether the node number is shown\\ \hline
    defaultSize &     float &      &     -1. &     global node size; if -1.f, node size is relative to openGL.initialMaxSceneSize\\ \hline
    defaultColor &     Float4 &     4 &     [0.2,0.2,1.,1.] &     default cRGB olor for nodes; 4th value is alpha-transparency\\ \hline
    showNodalSlopes &     Index &      &     False &     draw nodal slope vectors, e.g. in ANCF beam finite elements\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{VSettingsBeams}
Visualization settings for beam finite elements.\\ 
%
VSettingsBeams has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    axialTiling &     Index &      &     8 &     number of segments to discretise the beams axis\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{VSettingsBodies}
Visualization settings for bodies.\\ 
%
VSettingsBodies has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    show &     bool &      &     True &     flag to decide, whether the bodies are shown\\ \hline
    showNumbers &     bool &      &     False &     flag to decide, whether the body(=object) number is shown\\ \hline
    defaultSize &     Float3 &     3 &     [1.,1.,1.] &     global body size of xyz-cube\\ \hline
    defaultColor &     Float4 &     4 &     [0.2,0.2,1.,1.] &     default cRGB olor for bodies; 4th value is \\ \hline
    beams &     VSettingsBeams &      &      &     visualization settings for beams (e.g. ANCFCable or other beam elements)\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{VSettingsConnectors}
Visualization settings for connectors.\\ 
%
VSettingsConnectors has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    show &     bool &      &     True &     flag to decide, whether the connectors are shown\\ \hline
    showNumbers &     bool &      &     False &     flag to decide, whether the connector(=object) number is shown\\ \hline
    showContact &     bool &      &     False &     flag to decide, whether contact points, lines, etc. are shown\\ \hline
    defaultSize &     float &      &     0.1 &     global connector size; if -1.f, connector size is relative to maxSceneSize\\ \hline
    contactPointsDefaultSize &     float &      &     0.02 &     global contact points size; if -1.f, connector size is relative to maxSceneSize\\ \hline
    defaultColor &     Float4 &     4 &     [0.2,0.2,1.,1.] &     default cRGB olor for connectors; 4th value is alpha-transparency\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{VSettingsMarkers}
Visualization settings for markers.\\ 
%
VSettingsMarkers has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    show &     bool &      &     True &     flag to decide, whether the markers are shown\\ \hline
    showNumbers &     bool &      &     False &     flag to decide, whether the marker numbers are shown\\ \hline
    defaultSize &     float &      &     0.1 &     global marker size; if -1.f, marker size is relative to maxSceneSize\\ \hline
    defaultColor &     Float4 &     4 &     [0.1,0.5,0.1,1.] &     default cRGB olor for markers; 4th value is alpha-transparency\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{VSettingsLoads}
Visualization settings for loads.\\ 
%
VSettingsLoads has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    show &     bool &      &     True &     flag to decide, whether the loads are shown\\ \hline
    showNumbers &     bool &      &     False &     flag to decide, whether the load numbers are shown\\ \hline
    defaultSize &     float &      &     0.2 &     global load size; if -1.f, node size is relative to maxSceneSize\\ \hline
    fixedLoadSize &     bool &      &     True &     if true, the load is drawn with a fixed vector length in direction of the load vector, independently of the load size\\ \hline
    loadSizeFactor &     float &      &     0.1 &     if fixedLoadSize=false, then this scaling factor is used to draw the load vector\\ \hline
    defaultColor &     Float4 &     4 &     [0.7,0.1,0.1,1.] &     default cRGB olor for loads; 4th value is alpha-transparency\\ \hline
	  \end{longtable}
	\end{center}

%+++++++++++++++++++++++++++++++++++
\mysubsection{VisualizationSettings}
Settings for visualization. \\ 
%
VisualizationSettings has the following items:
%reference manual TABLE
\begin{center}
  \footnotesize
  \begin{longtable}{| p{4.5cm} | p{2.5cm} | p{0.5cm} | p{2.5cm} | p{6cm} |}
    \hline
    \bf Name & \bf type & \bf size & \bf default value & \bf description \\ \hline
    general &     VSettingsGeneral &      &      &     general visualization settings\\ \hline
    window &     VSettingsWindow &      &      &     visualization window and interaction settings\\ \hline
    openGL &     VSettingsOpenGL &      &      &     OpenGL rendering settings\\ \hline
    contour &     VSettingsContour &      &      &     contour plot visualization settings\\ \hline
    nodes &     VSettingsNodes &      &      &     node visualization settings\\ \hline
    bodies &     VSettingsBodies &      &      &     body visualization settings\\ \hline
    connectors &     VSettingsConnectors &      &      &     connector visualization settings\\ \hline
    markers &     VSettingsMarkers &      &      &     marker visualization settings\\ \hline
    loads &     VSettingsLoads &      &      &     load visualization settings\\ \hline
	  \end{longtable}
	\end{center}
