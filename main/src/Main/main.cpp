/** ***********************************************************************************************
* @file         main.cpp
* @brief
* @details		Details: experimental routines for development of Exudyn
*
* @author		Gerstmayr Johannes
* @date			2018-05-17 (generated)
* @pre			...
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See 'LICENSE.txt' for more details.
* @note			Bug reports, support and further information:
* 				- email: johannes.gerstmayr@uibk.ac.at
* 				- weblink: missing
* 				
*
************************************************************************************************ */


//++++++++++++++++++++++
//definition and includes in order to detect memory leaks in debug mode (dump memory leaks to Visual Studio after program exit)
#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  

#ifdef _WIN32
//that's Microsoft Visual Studio specific
#include <crtdbg.h>  
#endif
//++++++++++++++++++++++


#include <iostream> //for unit tests
#include <ostream>  //for unit tests
#include <fstream>  //for unit tests

#include "Linalg/ConstSizeVector.h" //linalg under development, general vector and matrix classes
#include "Utilities/ResizableArray.h" //linalg under development, general vector and matrix classes
#include "Linalg/SlimVector.h"
#include "Utilities/BasicFunctions.h"
#include "Utilities/ObjectContainer.h"


#ifdef PERFORM_UNIT_TESTS
//#include "lest/lest.hpp" //library to enable templated unit test definitions
#include "Tests/UnitTestBase.h" //for Exudyn tests
#endif



int main_() //main changed to "main_": no main needed in Python module!
{
    //enable memory leak checks by leak number {xxx}
#ifdef _DEBUG
#ifdef _WIN32
	int flag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	flag |= _CRTDBG_LEAK_CHECK_DF; // This turns on the leak-checking bit
	_CrtSetDbgFlag(flag);
	//_CrtSetBreakAlloc(366); //this will lead to a break-point where the memory leak happens
#endif
#endif



//#ifdef PERFORM_UNIT_TESTS
//
//    if(1)
//    {
//        //int returnValue = lest::run(specification, { "-p", "-v" }, std::cout);
//        //int returnValue = lest::run(specification, {"-p", "-v"}, file); //option "-p" to also report on passing tests!; option "-v" also to report on sections
//
//        UnitTestBase tests;
//        int returnValue = tests.PerformVectorAndArrayTests(1 * UnitTestFlags::reportOnPass + UnitTestFlags::reportSections + 0*UnitTestFlags::writeToCout);
//
//
//        if (returnValue) { std::cout << "unit tests FAILED\n"; }
//        if (!returnValue) { std::cout << "unit tests SUCCESSFUL\n"; }
//
//        std::cout << "Exudyn unit tests failcount=" << returnValue << "\n";
//
//        if (1) //write output to file
//        {
//            ofstream testfile("UnitTestsOutput.txt");
//
//            testfile << "Exudyn UNIT TESTS\n";
//            testfile << "*****************\n\n";
//            testfile << tests.GetOutputString().c_str() << "\n";
//        }
//
//    }
//#endif

    //std::cin.get();

    //_CrtDumpMemoryLeaks(); //dump memory leaks to Visual Studio after exit

#ifdef _WIN32
	_CrtDumpMemoryLeaks(); //dump memory leaks to Visual Studio after exit
#endif

    return 0;
}

