/** ***********************************************************************************************
* @file         stdoutput.h
* @brief
* @details		Details: externals which provide directives for output, error and warning messages
*
* @author		Gerstmayr Johannes
* @date			2019-04-02 (generated)
* @pre			...
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See 'LICENSE.txt' for more details.
* @note			Bug reports, support and further information:
* 				- email: johannes.gerstmayr@uibk.ac.at
* 				- weblink: missing
* 				
*
************************************************************************************************ */



#include <sstream>      // std::stringbuf
#include <iostream>     // std::cout, std::ostream

class OutputBuffer : public std::stringbuf //uses solution of so:redirect-stdcout-to-a-custom-writer
{
	std::string buf; //this buffer is used until end of line is detected
public:
	OutputBuffer() { setbuf(0, 0); } //this leads to an overflow in any access to stringbuf!
	//virtual int sync(); //this solution does not work!
	virtual int overflow(int c = EOF);
};

void SysError(std::string error_msg); //!< prints a formated system (inernal) error message (+log file, etc.); 'error_msg' shall only contain the error information, do not write "ERROR: ..." or similar

void PyError(std::string error_msg); //!< prints a formated python error message (+log file, etc.); 'error_msg' shall only contain the error information, do not write "Python ERROR: ..." or similar

void PyWarning(std::string warning_msg); //!< prints a formated python warning message (+log file, etc.); 'warning_msg' shall only contain the warning information, do not write "Python WARNING: ..." or similar

void PyGetCurrentFileInformation(std::string& fileName, Index& lineNumber); //!< retrieve current parsed file information from python (for error/warning messages...)

//********************************
extern std::ostream pout;  //!< provide a output stream (e.g. for Python); remove the following line if linkage to Python is not needed!
//alternatively use:
//#define pout std::cout
//********************************
