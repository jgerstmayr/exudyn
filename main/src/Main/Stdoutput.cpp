/** ***********************************************************************************************
* @file         stdoutput.cpp
* @brief
* @details		Details: externals which provide directives for output, error and warning messages
*				Here, the redirection goes to Python stream
*
* @author		Gerstmayr Johannes
* @date			2019-04-02 (generated)
* @pre			...
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See 'LICENSE.txt' for more details.
* @note			Bug reports, support and further information:
* 				- email: johannes.gerstmayr@uibk.ac.at
* 				- weblink: missing
* 				
*
************************************************************************************************ */


//#include "Main/stdoutput.h"
#include "Utilities/BasicDefinitions.h" //includes stdoutput.h
#include "Utilities/BasicFunctions.h"	//includes stdoutput.h
#include <chrono> //sleep_for()

#include <pybind11/pybind11.h>
#include <thread>
//#include <pybind11/stl.h>
//#include <pybind11/stl_bind.h>
//#include <pybind11/operators.h>
//#include <pybind11/numpy.h>
//does not work globally: #include <pybind11/iostream.h> //used to redirect cout:  py::scoped_ostream_redirect output;
//#include <pybind11/cast.h> //for arguments
//#include <pybind11/functional.h> //for functions
namespace py = pybind11;

//these two variables become global
OutputBuffer outputBuffer; //this is my customized output buffer, which can redirect the output stream;
std::ostream pout(&outputBuffer);  // link ostream pout to buffer; pout behaves the same as std::cout

//! used to print to python; string is temporary stored and written as soon as '\n' is detected
int OutputBuffer::overflow(int c)
{
	if ((char)c != '\n') {
		buf.push_back((char)c);
	}
	else {
		py::print(buf);
		buf.clear();
		//std::this_thread::sleep_for(std::chrono::milliseconds(10)); //add this to enable Spyder to print messages
	}
	//py::print((char)c);
	return c;
}

void PyGetCurrentFileInformation(std::string& fileName, Index& lineNumber) //!< retrieve current parsed file information from python (for error/warning messages...)
{
	py::module inspect = py::module::import("inspect");
	py::object currentFrame = inspect.attr("currentframe")();
	lineNumber = int(py::int_(currentFrame.attr("f_lineno")));

	//python usage: fn = inspect.getframeinfo(inspect.currentframe()).filename
	py::object frameInfo = inspect.attr("getframeinfo")(currentFrame);
	fileName = std::string(py::str(frameInfo.attr("filename")));

	//py::print(std::string("info has been called from: ") + fileName + std::string(" at line ") + EXUstd::ToString(line));
}

void PyError(std::string error_msg) //!< prints a formated error message (+log file, etc.); 'error_msg' shall only contain the error information, do not write "Python ERROR: ..." or similar
{
	STDstring fileName;
	Index lineNumber;
	PyGetCurrentFileInformation(fileName, lineNumber);

	pout << "\nPython ERROR [file '" << fileName << "', line " << lineNumber << "]: \n";
	pout << error_msg << "\n\n";
	PyErr_SetString(PyExc_RuntimeError, "Exudyn: parsing of python file terminated due to python (user) error");
}

void SysError(std::string error_msg) //!< prints a formated error message (+log file, etc.); 'error_msg' shall only contain the error information, do not write "Python ERROR: ..." or similar
{
	STDstring fileName;
	Index lineNumber;
	PyGetCurrentFileInformation(fileName, lineNumber);

	pout << "\nSYSTEM ERROR [file '" << fileName << "', line " << lineNumber << "]: \n";
	pout << error_msg << "\n\n";
	PyErr_SetString(PyExc_RuntimeError, "Exudyn: parsing of python file terminated due to system error");
}

void PyWarning(std::string warning_msg) //!< prints a formated warning message (+log file, etc.); 'warning_msg' shall only contain the warning information, do not write "Python WARNING: ..." or similar
{
	STDstring fileName;
	Index lineNumber;
	PyGetCurrentFileInformation(fileName, lineNumber);

	pout << "\nPython WARNING [file '" << fileName << "', line " << lineNumber << "]: \n";
	pout << warning_msg << "\n\n";
}

////this solution does not work!
//int OutputBuffer::sync() 
//{
//	//redirect stream "this->str()" to this buffer: 
//	//this could go to cout or any other output (e.g. Python)
//	//std::cout << this->str();
//	py::print(this->str());
//	py::print("test");
//	return 0;
//}

