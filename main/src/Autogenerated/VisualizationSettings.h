/** ***********************************************************************************************
* @class        VSettingsGeneral
* @brief        General settings for visualization.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class VSettingsGeneral // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  float graphicsUpdateInterval;                   //!< AUTO: interval of graphics update during simulation in seconds; 0.1 = 10 frames per second; low numbers might slow down computation speed
  bool autoFitScene;                              //!< AUTO: automatically fit scene within first second after StartRenderer()
  float textSize;                                 //!< AUTO: general text size if not overwritten
  float minSceneSize;                             //!< AUTO: minimum scene size for initial scene size and for autoFitScene, to avoid division by zero; SET GREATER THAN ZERO
  Float4 backgroundColor;                         //!< AUTO: red, green, blue and alpha values for background of render window (white=[1,1,1,1]; black = [0,0,0,1])
  float coordinateSystemSize;                     //!< AUTO: size of coordinate system relative to screen
  bool drawCoordinateSystem;                      //!< AUTO: false = no coordinate system shown
  bool showComputationInfo;                       //!< AUTO: false = no info about computation (current time, solver, etc.) shown
  float pointSize;                                //!< AUTO: global point size (absolute)
  Index circleTiling;                             //!< AUTO: global number of segments for circles; if smaller than 2, 2 segments are used (flat)
  Index cylinderTiling;                           //!< AUTO: global number of segments for cylinders; if smaller than 2, 2 segments are used (flat)
  Index sphereTiling;                             //!< AUTO: global number of segments for spheres; if smaller than 2, 2 segments are used (flat)

  //! AUTO: default constructor with parameter initialization
  VSettingsGeneral()
  {
    graphicsUpdateInterval = 0.1f;
    autoFitScene = true;
    textSize = 12.f;
    minSceneSize = 0.1f;
    backgroundColor = Float4({1.f,1.f,1.f,1.f});
    coordinateSystemSize = 0.4f;
    drawCoordinateSystem = true;
    showComputationInfo = true;
    pointSize = 0.01f;
    circleTiling = 16;
    cylinderTiling = 16;
    sphereTiling = 8;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual VSettingsGeneral* GetClone() const { return new VSettingsGeneral(*this); }
  
  //! AUTO: Read (Reference) access to: interval of graphics update during simulation in seconds; 0.1 = 10 frames per second; low numbers might slow down computation speed
  const float& GetGraphicsUpdateInterval() const { return graphicsUpdateInterval; }
  //! AUTO: Write (Reference) access to: interval of graphics update during simulation in seconds; 0.1 = 10 frames per second; low numbers might slow down computation speed
  float& GetGraphicsUpdateInterval() { return graphicsUpdateInterval; }

  //! AUTO: Read (Reference) access to: automatically fit scene within first second after StartRenderer()
  const bool& GetAutoFitScene() const { return autoFitScene; }
  //! AUTO: Write (Reference) access to: automatically fit scene within first second after StartRenderer()
  bool& GetAutoFitScene() { return autoFitScene; }

  //! AUTO: Read (Reference) access to: general text size if not overwritten
  const float& GetTextSize() const { return textSize; }
  //! AUTO: Write (Reference) access to: general text size if not overwritten
  float& GetTextSize() { return textSize; }

  //! AUTO: Read (Reference) access to: minimum scene size for initial scene size and for autoFitScene, to avoid division by zero; SET GREATER THAN ZERO
  const float& GetMinSceneSize() const { return minSceneSize; }
  //! AUTO: Write (Reference) access to: minimum scene size for initial scene size and for autoFitScene, to avoid division by zero; SET GREATER THAN ZERO
  float& GetMinSceneSize() { return minSceneSize; }

  //! AUTO: Read (Reference) access to: red, green, blue and alpha values for background of render window (white=[1,1,1,1]; black = [0,0,0,1])
  const Float4& GetBackgroundColor() const { return backgroundColor; }
  //! AUTO: Write (Reference) access to: red, green, blue and alpha values for background of render window (white=[1,1,1,1]; black = [0,0,0,1])
  Float4& GetBackgroundColor() { return backgroundColor; }
  //! AUTO: Set function (needed in pybind) for: red, green, blue and alpha values for background of render window (white=[1,1,1,1]; black = [0,0,0,1])
  void PySetBackgroundColor(const std::vector<float>& backgroundColorInit) { backgroundColor = backgroundColorInit; }
  //! AUTO: Read (Copy) access to: red, green, blue and alpha values for background of render window (white=[1,1,1,1]; black = [0,0,0,1])
  std::vector<float> PyGetBackgroundColor() const { return (std::vector<float>)backgroundColor; }

  //! AUTO: Read (Reference) access to: size of coordinate system relative to screen
  const float& GetCoordinateSystemSize() const { return coordinateSystemSize; }
  //! AUTO: Write (Reference) access to: size of coordinate system relative to screen
  float& GetCoordinateSystemSize() { return coordinateSystemSize; }

  //! AUTO: Read (Reference) access to: false = no coordinate system shown
  const bool& GetDrawCoordinateSystem() const { return drawCoordinateSystem; }
  //! AUTO: Write (Reference) access to: false = no coordinate system shown
  bool& GetDrawCoordinateSystem() { return drawCoordinateSystem; }

  //! AUTO: Read (Reference) access to: false = no info about computation (current time, solver, etc.) shown
  const bool& GetShowComputationInfo() const { return showComputationInfo; }
  //! AUTO: Write (Reference) access to: false = no info about computation (current time, solver, etc.) shown
  bool& GetShowComputationInfo() { return showComputationInfo; }

  //! AUTO: Read (Reference) access to: global point size (absolute)
  const float& GetPointSize() const { return pointSize; }
  //! AUTO: Write (Reference) access to: global point size (absolute)
  float& GetPointSize() { return pointSize; }

  //! AUTO: Read (Reference) access to: global number of segments for circles; if smaller than 2, 2 segments are used (flat)
  const Index& GetCircleTiling() const { return circleTiling; }
  //! AUTO: Write (Reference) access to: global number of segments for circles; if smaller than 2, 2 segments are used (flat)
  Index& GetCircleTiling() { return circleTiling; }

  //! AUTO: Read (Reference) access to: global number of segments for cylinders; if smaller than 2, 2 segments are used (flat)
  const Index& GetCylinderTiling() const { return cylinderTiling; }
  //! AUTO: Write (Reference) access to: global number of segments for cylinders; if smaller than 2, 2 segments are used (flat)
  Index& GetCylinderTiling() { return cylinderTiling; }

  //! AUTO: Read (Reference) access to: global number of segments for spheres; if smaller than 2, 2 segments are used (flat)
  const Index& GetSphereTiling() const { return sphereTiling; }
  //! AUTO: Write (Reference) access to: global number of segments for spheres; if smaller than 2, 2 segments are used (flat)
  Index& GetSphereTiling() { return sphereTiling; }

  virtual void Print(std::ostream& os) const
  {
    os << "VSettingsGeneral" << ":\n";
    os << "  graphicsUpdateInterval = " << graphicsUpdateInterval << "\n";
    os << "  autoFitScene = " << autoFitScene << "\n";
    os << "  textSize = " << textSize << "\n";
    os << "  minSceneSize = " << minSceneSize << "\n";
    os << "  backgroundColor = " << backgroundColor << "\n";
    os << "  coordinateSystemSize = " << coordinateSystemSize << "\n";
    os << "  drawCoordinateSystem = " << drawCoordinateSystem << "\n";
    os << "  showComputationInfo = " << showComputationInfo << "\n";
    os << "  pointSize = " << pointSize << "\n";
    os << "  circleTiling = " << circleTiling << "\n";
    os << "  cylinderTiling = " << cylinderTiling << "\n";
    os << "  sphereTiling = " << sphereTiling << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const VSettingsGeneral& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        VSettingsWindow
* @brief        Window and interaction settings for visualization; handle changes with care, as they might lead to unexpected results or crashes.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class VSettingsWindow // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  Index2 renderWindowSize;                        //!< AUTO: initial size of OpenGL render window in pixel
  Index startupTimeout;                           //!< AUTO: OpenGL render window startup timeout in ms (change might be necessary if CPU is very slow)
  float keypressRotationStep;                     //!< AUTO: rotation increment per keypress in degree (full rotation = 360 degree)
  float mouseMoveRotationFactor;                  //!< AUTO: rotation increment per 1 pixel mouse movement in degree
  float keypressTranslationStep;                  //!< AUTO: translation increment per keypress relative to window size
  float zoomStepFactor;                           //!< AUTO: change of zoom per keypress (keypad +/-) or mouse wheel increment

  //! AUTO: default constructor with parameter initialization
  VSettingsWindow()
  {
    renderWindowSize = Index2({1024,768});
    startupTimeout = 5000;
    keypressRotationStep = 5.f;
    mouseMoveRotationFactor = 1.f;
    keypressTranslationStep = 0.1f;
    zoomStepFactor = 1.15f;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual VSettingsWindow* GetClone() const { return new VSettingsWindow(*this); }
  
  //! AUTO: Read (Reference) access to: initial size of OpenGL render window in pixel
  const Index2& GetRenderWindowSize() const { return renderWindowSize; }
  //! AUTO: Write (Reference) access to: initial size of OpenGL render window in pixel
  Index2& GetRenderWindowSize() { return renderWindowSize; }
  //! AUTO: Set function (needed in pybind) for: initial size of OpenGL render window in pixel
  void PySetRenderWindowSize(const std::vector<Index>& renderWindowSizeInit) { renderWindowSize = renderWindowSizeInit; }
  //! AUTO: Read (Copy) access to: initial size of OpenGL render window in pixel
  std::vector<Index> PyGetRenderWindowSize() const { return (std::vector<Index>)renderWindowSize; }

  //! AUTO: Read (Reference) access to: OpenGL render window startup timeout in ms (change might be necessary if CPU is very slow)
  const Index& GetStartupTimeout() const { return startupTimeout; }
  //! AUTO: Write (Reference) access to: OpenGL render window startup timeout in ms (change might be necessary if CPU is very slow)
  Index& GetStartupTimeout() { return startupTimeout; }

  //! AUTO: Read (Reference) access to: rotation increment per keypress in degree (full rotation = 360 degree)
  const float& GetKeypressRotationStep() const { return keypressRotationStep; }
  //! AUTO: Write (Reference) access to: rotation increment per keypress in degree (full rotation = 360 degree)
  float& GetKeypressRotationStep() { return keypressRotationStep; }

  //! AUTO: Read (Reference) access to: rotation increment per 1 pixel mouse movement in degree
  const float& GetMouseMoveRotationFactor() const { return mouseMoveRotationFactor; }
  //! AUTO: Write (Reference) access to: rotation increment per 1 pixel mouse movement in degree
  float& GetMouseMoveRotationFactor() { return mouseMoveRotationFactor; }

  //! AUTO: Read (Reference) access to: translation increment per keypress relative to window size
  const float& GetKeypressTranslationStep() const { return keypressTranslationStep; }
  //! AUTO: Write (Reference) access to: translation increment per keypress relative to window size
  float& GetKeypressTranslationStep() { return keypressTranslationStep; }

  //! AUTO: Read (Reference) access to: change of zoom per keypress (keypad +/-) or mouse wheel increment
  const float& GetZoomStepFactor() const { return zoomStepFactor; }
  //! AUTO: Write (Reference) access to: change of zoom per keypress (keypad +/-) or mouse wheel increment
  float& GetZoomStepFactor() { return zoomStepFactor; }

  virtual void Print(std::ostream& os) const
  {
    os << "VSettingsWindow" << ":\n";
    os << "  renderWindowSize = " << renderWindowSize << "\n";
    os << "  startupTimeout = " << startupTimeout << "\n";
    os << "  keypressRotationStep = " << keypressRotationStep << "\n";
    os << "  mouseMoveRotationFactor = " << mouseMoveRotationFactor << "\n";
    os << "  keypressTranslationStep = " << keypressTranslationStep << "\n";
    os << "  zoomStepFactor = " << zoomStepFactor << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const VSettingsWindow& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        VSettingsOpenGL
* @brief        OpenGL settings for 2D and 2D rendering.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class VSettingsOpenGL // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  Float3 initialCenterPoint;                      //!< AUTO: centerpoint of scene (3D) at renderer startup; overwritten if autoFitScene = True
  float initialZoom;                              //!< AUTO: initial zoom of scene; overwritten/ignored if autoFitScene = True
  float initialMaxSceneSize;                      //!< AUTO: initial maximum scene size (auto: diagonal of cube with maximum scene coordinates); used for 'zoom all' functionality and for visibility of objects; overwritten if autoFitScene = True
  Float9 initialModelRotation;                    //!< AUTO: initial model rotation matrix for OpenGl; floats are stored in raw-major format
  Index multiSampling;                            //!< AUTO: multi sampling turned off (<=1) or turned on to given values (2, 4, 8 or 16); increases the graphics buffers and might crash due to graphics card memory limitations; only works if supported by hardware; if it does not work, try to change 3D graphics hardware settings!
  float lineWidth;                                //!< AUTO: width of lines used for representation of lines, circles, points, etc.
  bool lineSmooth;                                //!< AUTO: draw lines smooth
  float textLineWidth;                            //!< AUTO: width of lines used for representation of text
  bool textLineSmooth;                            //!< AUTO: draw lines for representation of text smooth
  bool showFaces;                                 //!< AUTO: show faces of triangles, etc.; using the options showFaces=false and showFaceEdges=true gives are wire frame representation
  bool showFaceEdges;                             //!< AUTO: show edges of faces; using the options showFaces=false and showFaceEdges=true gives are wire frame representation
  bool shadeModelSmooth;                          //!< AUTO: true: turn on smoothing for shaders, which uses vertex normals to smooth surfaces
  Float4 materialSpecular;                        //!< AUTO: 4f specular color of material
  float materialShininess;                        //!< AUTO: shininess of material
  bool enableLight0;                              //!< AUTO: turn on/off light0
  Float4 light0position;                          //!< AUTO: 4f position vector of GL light0; 4th value should be 0, otherwise the vector obtains a special interpretation, see opengl manuals
  float light0ambient;                            //!< AUTO: ambient value of GL light0
  float light0diffuse;                            //!< AUTO: diffuse value of GL light0
  float light0specular;                           //!< AUTO: specular value of GL light0
  bool enableLight1;                              //!< AUTO: turn on/off light1
  Float4 light1position;                          //!< AUTO: 4f position vector of GL light1; 4th value should be 0, otherwise the vector obtains a special interpretation, see opengl manuals
  float light1ambient;                            //!< AUTO: ambient value of GL light1
  float light1diffuse;                            //!< AUTO: diffuse value of GL light1
  float light1specular;                           //!< AUTO: specular value of GL light1
  bool drawFaceNormals;                           //!< AUTO: draws triangle normals, e.g. at center of triangles; used for debugging of faces
  bool drawVertexNormals;                         //!< AUTO: draws vertex normals; used for debugging
  float drawNormalsLength;                        //!< AUTO: length of normals; used for debugging

  //! AUTO: default constructor with parameter initialization
  VSettingsOpenGL()
  {
    initialCenterPoint = Float3({0.f,0.f,0.f});
    initialZoom = 1.f;
    initialMaxSceneSize = 1.f;
    initialModelRotation = Float9({1.f,0.f,0.f, 0.f,1.f,0.f, 0.f,0.f,1.f});
    multiSampling = 1;
    lineWidth = 1.f;
    lineSmooth = true;
    textLineWidth = 1.f;
    textLineSmooth = false;
    showFaces = true;
    showFaceEdges = false;
    shadeModelSmooth = true;
    materialSpecular = Float4({1.f,1.f,1.f,1.f});
    materialShininess = 60.f;
    enableLight0 = true;
    light0position = Float4({1.f,1.f,-1.f,0.f});
    light0ambient = 0.25f;
    light0diffuse = 0.4f;
    light0specular = 0.4f;
    enableLight1 = true;
    light1position = Float4({0.f,3.f,2.f,0.f});
    light1ambient = 0.25f;
    light1diffuse = 0.4f;
    light1specular = 0.f;
    drawFaceNormals = false;
    drawVertexNormals = false;
    drawNormalsLength = 0.1f;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual VSettingsOpenGL* GetClone() const { return new VSettingsOpenGL(*this); }
  
  //! AUTO: Read (Reference) access to: centerpoint of scene (3D) at renderer startup; overwritten if autoFitScene = True
  const Float3& GetInitialCenterPoint() const { return initialCenterPoint; }
  //! AUTO: Write (Reference) access to: centerpoint of scene (3D) at renderer startup; overwritten if autoFitScene = True
  Float3& GetInitialCenterPoint() { return initialCenterPoint; }
  //! AUTO: Set function (needed in pybind) for: centerpoint of scene (3D) at renderer startup; overwritten if autoFitScene = True
  void PySetInitialCenterPoint(const std::vector<float>& initialCenterPointInit) { initialCenterPoint = initialCenterPointInit; }
  //! AUTO: Read (Copy) access to: centerpoint of scene (3D) at renderer startup; overwritten if autoFitScene = True
  std::vector<float> PyGetInitialCenterPoint() const { return (std::vector<float>)initialCenterPoint; }

  //! AUTO: Read (Reference) access to: initial zoom of scene; overwritten/ignored if autoFitScene = True
  const float& GetInitialZoom() const { return initialZoom; }
  //! AUTO: Write (Reference) access to: initial zoom of scene; overwritten/ignored if autoFitScene = True
  float& GetInitialZoom() { return initialZoom; }

  //! AUTO: Read (Reference) access to: initial maximum scene size (auto: diagonal of cube with maximum scene coordinates); used for 'zoom all' functionality and for visibility of objects; overwritten if autoFitScene = True
  const float& GetInitialMaxSceneSize() const { return initialMaxSceneSize; }
  //! AUTO: Write (Reference) access to: initial maximum scene size (auto: diagonal of cube with maximum scene coordinates); used for 'zoom all' functionality and for visibility of objects; overwritten if autoFitScene = True
  float& GetInitialMaxSceneSize() { return initialMaxSceneSize; }

  //! AUTO: Read (Reference) access to: initial model rotation matrix for OpenGl; floats are stored in raw-major format
  const Float9& GetInitialModelRotation() const { return initialModelRotation; }
  //! AUTO: Write (Reference) access to: initial model rotation matrix for OpenGl; floats are stored in raw-major format
  Float9& GetInitialModelRotation() { return initialModelRotation; }
  //! AUTO: Set function (needed in pybind) for: initial model rotation matrix for OpenGl; floats are stored in raw-major format
  void PySetInitialModelRotation(const std::vector<float>& initialModelRotationInit) { initialModelRotation = initialModelRotationInit; }
  //! AUTO: Read (Copy) access to: initial model rotation matrix for OpenGl; floats are stored in raw-major format
  std::vector<float> PyGetInitialModelRotation() const { return (std::vector<float>)initialModelRotation; }

  //! AUTO: Read (Reference) access to: multi sampling turned off (<=1) or turned on to given values (2, 4, 8 or 16); increases the graphics buffers and might crash due to graphics card memory limitations; only works if supported by hardware; if it does not work, try to change 3D graphics hardware settings!
  const Index& GetMultiSampling() const { return multiSampling; }
  //! AUTO: Write (Reference) access to: multi sampling turned off (<=1) or turned on to given values (2, 4, 8 or 16); increases the graphics buffers and might crash due to graphics card memory limitations; only works if supported by hardware; if it does not work, try to change 3D graphics hardware settings!
  Index& GetMultiSampling() { return multiSampling; }

  //! AUTO: Read (Reference) access to: width of lines used for representation of lines, circles, points, etc.
  const float& GetLineWidth() const { return lineWidth; }
  //! AUTO: Write (Reference) access to: width of lines used for representation of lines, circles, points, etc.
  float& GetLineWidth() { return lineWidth; }

  //! AUTO: Read (Reference) access to: draw lines smooth
  const bool& GetLineSmooth() const { return lineSmooth; }
  //! AUTO: Write (Reference) access to: draw lines smooth
  bool& GetLineSmooth() { return lineSmooth; }

  //! AUTO: Read (Reference) access to: width of lines used for representation of text
  const float& GetTextLineWidth() const { return textLineWidth; }
  //! AUTO: Write (Reference) access to: width of lines used for representation of text
  float& GetTextLineWidth() { return textLineWidth; }

  //! AUTO: Read (Reference) access to: draw lines for representation of text smooth
  const bool& GetTextLineSmooth() const { return textLineSmooth; }
  //! AUTO: Write (Reference) access to: draw lines for representation of text smooth
  bool& GetTextLineSmooth() { return textLineSmooth; }

  //! AUTO: Read (Reference) access to: show faces of triangles, etc.; using the options showFaces=false and showFaceEdges=true gives are wire frame representation
  const bool& GetShowFaces() const { return showFaces; }
  //! AUTO: Write (Reference) access to: show faces of triangles, etc.; using the options showFaces=false and showFaceEdges=true gives are wire frame representation
  bool& GetShowFaces() { return showFaces; }

  //! AUTO: Read (Reference) access to: show edges of faces; using the options showFaces=false and showFaceEdges=true gives are wire frame representation
  const bool& GetShowFaceEdges() const { return showFaceEdges; }
  //! AUTO: Write (Reference) access to: show edges of faces; using the options showFaces=false and showFaceEdges=true gives are wire frame representation
  bool& GetShowFaceEdges() { return showFaceEdges; }

  //! AUTO: Read (Reference) access to: true: turn on smoothing for shaders, which uses vertex normals to smooth surfaces
  const bool& GetShadeModelSmooth() const { return shadeModelSmooth; }
  //! AUTO: Write (Reference) access to: true: turn on smoothing for shaders, which uses vertex normals to smooth surfaces
  bool& GetShadeModelSmooth() { return shadeModelSmooth; }

  //! AUTO: Read (Reference) access to: 4f specular color of material
  const Float4& GetMaterialSpecular() const { return materialSpecular; }
  //! AUTO: Write (Reference) access to: 4f specular color of material
  Float4& GetMaterialSpecular() { return materialSpecular; }
  //! AUTO: Set function (needed in pybind) for: 4f specular color of material
  void PySetMaterialSpecular(const std::vector<float>& materialSpecularInit) { materialSpecular = materialSpecularInit; }
  //! AUTO: Read (Copy) access to: 4f specular color of material
  std::vector<float> PyGetMaterialSpecular() const { return (std::vector<float>)materialSpecular; }

  //! AUTO: Read (Reference) access to: shininess of material
  const float& GetMaterialShininess() const { return materialShininess; }
  //! AUTO: Write (Reference) access to: shininess of material
  float& GetMaterialShininess() { return materialShininess; }

  //! AUTO: Read (Reference) access to: turn on/off light0
  const bool& GetEnableLight0() const { return enableLight0; }
  //! AUTO: Write (Reference) access to: turn on/off light0
  bool& GetEnableLight0() { return enableLight0; }

  //! AUTO: Read (Reference) access to: 4f position vector of GL light0; 4th value should be 0, otherwise the vector obtains a special interpretation, see opengl manuals
  const Float4& GetLight0position() const { return light0position; }
  //! AUTO: Write (Reference) access to: 4f position vector of GL light0; 4th value should be 0, otherwise the vector obtains a special interpretation, see opengl manuals
  Float4& GetLight0position() { return light0position; }
  //! AUTO: Set function (needed in pybind) for: 4f position vector of GL light0; 4th value should be 0, otherwise the vector obtains a special interpretation, see opengl manuals
  void PySetLight0position(const std::vector<float>& light0positionInit) { light0position = light0positionInit; }
  //! AUTO: Read (Copy) access to: 4f position vector of GL light0; 4th value should be 0, otherwise the vector obtains a special interpretation, see opengl manuals
  std::vector<float> PyGetLight0position() const { return (std::vector<float>)light0position; }

  //! AUTO: Read (Reference) access to: ambient value of GL light0
  const float& GetLight0ambient() const { return light0ambient; }
  //! AUTO: Write (Reference) access to: ambient value of GL light0
  float& GetLight0ambient() { return light0ambient; }

  //! AUTO: Read (Reference) access to: diffuse value of GL light0
  const float& GetLight0diffuse() const { return light0diffuse; }
  //! AUTO: Write (Reference) access to: diffuse value of GL light0
  float& GetLight0diffuse() { return light0diffuse; }

  //! AUTO: Read (Reference) access to: specular value of GL light0
  const float& GetLight0specular() const { return light0specular; }
  //! AUTO: Write (Reference) access to: specular value of GL light0
  float& GetLight0specular() { return light0specular; }

  //! AUTO: Read (Reference) access to: turn on/off light1
  const bool& GetEnableLight1() const { return enableLight1; }
  //! AUTO: Write (Reference) access to: turn on/off light1
  bool& GetEnableLight1() { return enableLight1; }

  //! AUTO: Read (Reference) access to: 4f position vector of GL light1; 4th value should be 0, otherwise the vector obtains a special interpretation, see opengl manuals
  const Float4& GetLight1position() const { return light1position; }
  //! AUTO: Write (Reference) access to: 4f position vector of GL light1; 4th value should be 0, otherwise the vector obtains a special interpretation, see opengl manuals
  Float4& GetLight1position() { return light1position; }
  //! AUTO: Set function (needed in pybind) for: 4f position vector of GL light1; 4th value should be 0, otherwise the vector obtains a special interpretation, see opengl manuals
  void PySetLight1position(const std::vector<float>& light1positionInit) { light1position = light1positionInit; }
  //! AUTO: Read (Copy) access to: 4f position vector of GL light1; 4th value should be 0, otherwise the vector obtains a special interpretation, see opengl manuals
  std::vector<float> PyGetLight1position() const { return (std::vector<float>)light1position; }

  //! AUTO: Read (Reference) access to: ambient value of GL light1
  const float& GetLight1ambient() const { return light1ambient; }
  //! AUTO: Write (Reference) access to: ambient value of GL light1
  float& GetLight1ambient() { return light1ambient; }

  //! AUTO: Read (Reference) access to: diffuse value of GL light1
  const float& GetLight1diffuse() const { return light1diffuse; }
  //! AUTO: Write (Reference) access to: diffuse value of GL light1
  float& GetLight1diffuse() { return light1diffuse; }

  //! AUTO: Read (Reference) access to: specular value of GL light1
  const float& GetLight1specular() const { return light1specular; }
  //! AUTO: Write (Reference) access to: specular value of GL light1
  float& GetLight1specular() { return light1specular; }

  //! AUTO: Read (Reference) access to: draws triangle normals, e.g. at center of triangles; used for debugging of faces
  const bool& GetDrawFaceNormals() const { return drawFaceNormals; }
  //! AUTO: Write (Reference) access to: draws triangle normals, e.g. at center of triangles; used for debugging of faces
  bool& GetDrawFaceNormals() { return drawFaceNormals; }

  //! AUTO: Read (Reference) access to: draws vertex normals; used for debugging
  const bool& GetDrawVertexNormals() const { return drawVertexNormals; }
  //! AUTO: Write (Reference) access to: draws vertex normals; used for debugging
  bool& GetDrawVertexNormals() { return drawVertexNormals; }

  //! AUTO: Read (Reference) access to: length of normals; used for debugging
  const float& GetDrawNormalsLength() const { return drawNormalsLength; }
  //! AUTO: Write (Reference) access to: length of normals; used for debugging
  float& GetDrawNormalsLength() { return drawNormalsLength; }

  virtual void Print(std::ostream& os) const
  {
    os << "VSettingsOpenGL" << ":\n";
    os << "  initialCenterPoint = " << initialCenterPoint << "\n";
    os << "  initialZoom = " << initialZoom << "\n";
    os << "  initialMaxSceneSize = " << initialMaxSceneSize << "\n";
    os << "  initialModelRotation = " << initialModelRotation << "\n";
    os << "  multiSampling = " << multiSampling << "\n";
    os << "  lineWidth = " << lineWidth << "\n";
    os << "  lineSmooth = " << lineSmooth << "\n";
    os << "  textLineWidth = " << textLineWidth << "\n";
    os << "  textLineSmooth = " << textLineSmooth << "\n";
    os << "  showFaces = " << showFaces << "\n";
    os << "  showFaceEdges = " << showFaceEdges << "\n";
    os << "  shadeModelSmooth = " << shadeModelSmooth << "\n";
    os << "  materialSpecular = " << materialSpecular << "\n";
    os << "  materialShininess = " << materialShininess << "\n";
    os << "  enableLight0 = " << enableLight0 << "\n";
    os << "  light0position = " << light0position << "\n";
    os << "  light0ambient = " << light0ambient << "\n";
    os << "  light0diffuse = " << light0diffuse << "\n";
    os << "  light0specular = " << light0specular << "\n";
    os << "  enableLight1 = " << enableLight1 << "\n";
    os << "  light1position = " << light1position << "\n";
    os << "  light1ambient = " << light1ambient << "\n";
    os << "  light1diffuse = " << light1diffuse << "\n";
    os << "  light1specular = " << light1specular << "\n";
    os << "  drawFaceNormals = " << drawFaceNormals << "\n";
    os << "  drawVertexNormals = " << drawVertexNormals << "\n";
    os << "  drawNormalsLength = " << drawNormalsLength << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const VSettingsOpenGL& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        VSettingsContour
* @brief        Settings for contour plots; use these options to visualize field data, such as displacements, stresses, strains, etc. for bodies, nodes and finite elements.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class VSettingsContour // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  Index outputVariableComponent;                  //!< AUTO: select the component of the chosen output variable; e.g., for displacements, 3 components are available: 0 == x, 1 == y, 2 == z component; if this component is not available by certain objects or nodes, no value is drawn
  OutputVariableType outputVariable;              //!< AUTO: selected contour plot output variable type; select OutputVariableType.None to deactivate contour plotting.
  float minValue;                                 //!< AUTO: minimum value for contour plot; set manually, if automaticRange == False
  float maxValue;                                 //!< AUTO: maximum value for contour plot; set manually, if automaticRange == False
  bool automaticRange;                            //!< AUTO: if true, the contour plot value range is chosen automatically to the maximum range
  bool showColorBar;                              //!< AUTO: show the colour bar with minimum and maximum values for the contour plot
  Index colorBarTiling;                           //!< AUTO: number of tiles (segements) shown in the colorbar for the contour plot

  //! AUTO: default constructor with parameter initialization
  VSettingsContour()
  {
    outputVariableComponent = 0;
    outputVariable = OutputVariableType::None;
    minValue = 0;
    maxValue = 1;
    automaticRange = true;
    showColorBar = true;
    colorBarTiling = 12;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual VSettingsContour* GetClone() const { return new VSettingsContour(*this); }
  
  //! AUTO: Read (Reference) access to: select the component of the chosen output variable; e.g., for displacements, 3 components are available: 0 == x, 1 == y, 2 == z component; if this component is not available by certain objects or nodes, no value is drawn
  const Index& GetOutputVariableComponent() const { return outputVariableComponent; }
  //! AUTO: Write (Reference) access to: select the component of the chosen output variable; e.g., for displacements, 3 components are available: 0 == x, 1 == y, 2 == z component; if this component is not available by certain objects or nodes, no value is drawn
  Index& GetOutputVariableComponent() { return outputVariableComponent; }

  //! AUTO: Read (Reference) access to: selected contour plot output variable type; select OutputVariableType.None to deactivate contour plotting.
  const OutputVariableType& GetOutputVariable() const { return outputVariable; }
  //! AUTO: Write (Reference) access to: selected contour plot output variable type; select OutputVariableType.None to deactivate contour plotting.
  OutputVariableType& GetOutputVariable() { return outputVariable; }

  //! AUTO: Read (Reference) access to: minimum value for contour plot; set manually, if automaticRange == False
  const float& GetMinValue() const { return minValue; }
  //! AUTO: Write (Reference) access to: minimum value for contour plot; set manually, if automaticRange == False
  float& GetMinValue() { return minValue; }

  //! AUTO: Read (Reference) access to: maximum value for contour plot; set manually, if automaticRange == False
  const float& GetMaxValue() const { return maxValue; }
  //! AUTO: Write (Reference) access to: maximum value for contour plot; set manually, if automaticRange == False
  float& GetMaxValue() { return maxValue; }

  //! AUTO: Read (Reference) access to: if true, the contour plot value range is chosen automatically to the maximum range
  const bool& GetAutomaticRange() const { return automaticRange; }
  //! AUTO: Write (Reference) access to: if true, the contour plot value range is chosen automatically to the maximum range
  bool& GetAutomaticRange() { return automaticRange; }

  //! AUTO: Read (Reference) access to: show the colour bar with minimum and maximum values for the contour plot
  const bool& GetShowColorBar() const { return showColorBar; }
  //! AUTO: Write (Reference) access to: show the colour bar with minimum and maximum values for the contour plot
  bool& GetShowColorBar() { return showColorBar; }

  //! AUTO: Read (Reference) access to: number of tiles (segements) shown in the colorbar for the contour plot
  const Index& GetColorBarTiling() const { return colorBarTiling; }
  //! AUTO: Write (Reference) access to: number of tiles (segements) shown in the colorbar for the contour plot
  Index& GetColorBarTiling() { return colorBarTiling; }

  virtual void Print(std::ostream& os) const
  {
    os << "VSettingsContour" << ":\n";
    os << "  outputVariableComponent = " << outputVariableComponent << "\n";
    os << "  outputVariable = " << GetOutputVariableTypeString(outputVariable) << "\n";
    os << "  minValue = " << minValue << "\n";
    os << "  maxValue = " << maxValue << "\n";
    os << "  automaticRange = " << automaticRange << "\n";
    os << "  showColorBar = " << showColorBar << "\n";
    os << "  colorBarTiling = " << colorBarTiling << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const VSettingsContour& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        VSettingsNodes
* @brief        Visualization settings for nodes.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class VSettingsNodes // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  bool show;                                      //!< AUTO: flag to decide, whether the nodes are shown
  bool showNumbers;                               //!< AUTO: flag to decide, whether the node number is shown
  float defaultSize;                              //!< AUTO: global node size; if -1.f, node size is relative to openGL.initialMaxSceneSize
  Float4 defaultColor;                            //!< AUTO: default cRGB olor for nodes; 4th value is alpha-transparency
  Index showNodalSlopes;                          //!< AUTO: draw nodal slope vectors, e.g. in ANCF beam finite elements

  //! AUTO: default constructor with parameter initialization
  VSettingsNodes()
  {
    show = true;
    showNumbers = false;
    defaultSize = -1.f;
    defaultColor = Float4({0.2f,0.2f,1.f,1.f});
    showNodalSlopes = false;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual VSettingsNodes* GetClone() const { return new VSettingsNodes(*this); }
  
  //! AUTO: Read (Reference) access to: flag to decide, whether the nodes are shown
  const bool& GetShow() const { return show; }
  //! AUTO: Write (Reference) access to: flag to decide, whether the nodes are shown
  bool& GetShow() { return show; }

  //! AUTO: Read (Reference) access to: flag to decide, whether the node number is shown
  const bool& GetShowNumbers() const { return showNumbers; }
  //! AUTO: Write (Reference) access to: flag to decide, whether the node number is shown
  bool& GetShowNumbers() { return showNumbers; }

  //! AUTO: Read (Reference) access to: global node size; if -1.f, node size is relative to openGL.initialMaxSceneSize
  const float& GetDefaultSize() const { return defaultSize; }
  //! AUTO: Write (Reference) access to: global node size; if -1.f, node size is relative to openGL.initialMaxSceneSize
  float& GetDefaultSize() { return defaultSize; }

  //! AUTO: Read (Reference) access to: default cRGB olor for nodes; 4th value is alpha-transparency
  const Float4& GetDefaultColor() const { return defaultColor; }
  //! AUTO: Write (Reference) access to: default cRGB olor for nodes; 4th value is alpha-transparency
  Float4& GetDefaultColor() { return defaultColor; }
  //! AUTO: Set function (needed in pybind) for: default cRGB olor for nodes; 4th value is alpha-transparency
  void PySetDefaultColor(const std::vector<float>& defaultColorInit) { defaultColor = defaultColorInit; }
  //! AUTO: Read (Copy) access to: default cRGB olor for nodes; 4th value is alpha-transparency
  std::vector<float> PyGetDefaultColor() const { return (std::vector<float>)defaultColor; }

  //! AUTO: Read (Reference) access to: draw nodal slope vectors, e.g. in ANCF beam finite elements
  const Index& GetShowNodalSlopes() const { return showNodalSlopes; }
  //! AUTO: Write (Reference) access to: draw nodal slope vectors, e.g. in ANCF beam finite elements
  Index& GetShowNodalSlopes() { return showNodalSlopes; }

  virtual void Print(std::ostream& os) const
  {
    os << "VSettingsNodes" << ":\n";
    os << "  show = " << show << "\n";
    os << "  showNumbers = " << showNumbers << "\n";
    os << "  defaultSize = " << defaultSize << "\n";
    os << "  defaultColor = " << defaultColor << "\n";
    os << "  showNodalSlopes = " << showNodalSlopes << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const VSettingsNodes& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        VSettingsBeams
* @brief        Visualization settings for beam finite elements.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class VSettingsBeams // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  Index axialTiling;                              //!< AUTO: number of segments to discretise the beams axis

  //! AUTO: default constructor with parameter initialization
  VSettingsBeams()
  {
    axialTiling = 8;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual VSettingsBeams* GetClone() const { return new VSettingsBeams(*this); }
  
  //! AUTO: Read (Reference) access to: number of segments to discretise the beams axis
  const Index& GetAxialTiling() const { return axialTiling; }
  //! AUTO: Write (Reference) access to: number of segments to discretise the beams axis
  Index& GetAxialTiling() { return axialTiling; }

  virtual void Print(std::ostream& os) const
  {
    os << "VSettingsBeams" << ":\n";
    os << "  axialTiling = " << axialTiling << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const VSettingsBeams& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        VSettingsBodies
* @brief        Visualization settings for bodies.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class VSettingsBodies // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  bool show;                                      //!< AUTO: flag to decide, whether the bodies are shown
  bool showNumbers;                               //!< AUTO: flag to decide, whether the body(=object) number is shown
  Float3 defaultSize;                             //!< AUTO: global body size of xyz-cube
  Float4 defaultColor;                            //!< AUTO: default cRGB olor for bodies; 4th value is 
  VSettingsBeams beams;                           //!< AUTO: visualization settings for beams (e.g. ANCFCable or other beam elements)

  //! AUTO: default constructor with parameter initialization
  VSettingsBodies()
  {
    show = true;
    showNumbers = false;
    defaultSize = Float3({1.f,1.f,1.f});
    defaultColor = Float4({0.2f,0.2f,1.f,1.f});
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual VSettingsBodies* GetClone() const { return new VSettingsBodies(*this); }
  
  //! AUTO: Read (Reference) access to: flag to decide, whether the bodies are shown
  const bool& GetShow() const { return show; }
  //! AUTO: Write (Reference) access to: flag to decide, whether the bodies are shown
  bool& GetShow() { return show; }

  //! AUTO: Read (Reference) access to: flag to decide, whether the body(=object) number is shown
  const bool& GetShowNumbers() const { return showNumbers; }
  //! AUTO: Write (Reference) access to: flag to decide, whether the body(=object) number is shown
  bool& GetShowNumbers() { return showNumbers; }

  //! AUTO: Read (Reference) access to: global body size of xyz-cube
  const Float3& GetDefaultSize() const { return defaultSize; }
  //! AUTO: Write (Reference) access to: global body size of xyz-cube
  Float3& GetDefaultSize() { return defaultSize; }
  //! AUTO: Set function (needed in pybind) for: global body size of xyz-cube
  void PySetDefaultSize(const std::vector<float>& defaultSizeInit) { defaultSize = defaultSizeInit; }
  //! AUTO: Read (Copy) access to: global body size of xyz-cube
  std::vector<float> PyGetDefaultSize() const { return (std::vector<float>)defaultSize; }

  //! AUTO: Read (Reference) access to: default cRGB olor for bodies; 4th value is 
  const Float4& GetDefaultColor() const { return defaultColor; }
  //! AUTO: Write (Reference) access to: default cRGB olor for bodies; 4th value is 
  Float4& GetDefaultColor() { return defaultColor; }
  //! AUTO: Set function (needed in pybind) for: default cRGB olor for bodies; 4th value is 
  void PySetDefaultColor(const std::vector<float>& defaultColorInit) { defaultColor = defaultColorInit; }
  //! AUTO: Read (Copy) access to: default cRGB olor for bodies; 4th value is 
  std::vector<float> PyGetDefaultColor() const { return (std::vector<float>)defaultColor; }

  //! AUTO: Read (Reference) access to: visualization settings for beams (e.g. ANCFCable or other beam elements)
  const VSettingsBeams& GetBeams() const { return beams; }
  //! AUTO: Write (Reference) access to: visualization settings for beams (e.g. ANCFCable or other beam elements)
  VSettingsBeams& GetBeams() { return beams; }

  virtual void Print(std::ostream& os) const
  {
    os << "VSettingsBodies" << ":\n";
    os << "  show = " << show << "\n";
    os << "  showNumbers = " << showNumbers << "\n";
    os << "  defaultSize = " << defaultSize << "\n";
    os << "  defaultColor = " << defaultColor << "\n";
    os << "  beams = " << beams << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const VSettingsBodies& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        VSettingsConnectors
* @brief        Visualization settings for connectors.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class VSettingsConnectors // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  bool show;                                      //!< AUTO: flag to decide, whether the connectors are shown
  bool showNumbers;                               //!< AUTO: flag to decide, whether the connector(=object) number is shown
  bool showContact;                               //!< AUTO: flag to decide, whether contact points, lines, etc. are shown
  float defaultSize;                              //!< AUTO: global connector size; if -1.f, connector size is relative to maxSceneSize
  float contactPointsDefaultSize;                 //!< AUTO: global contact points size; if -1.f, connector size is relative to maxSceneSize
  Float4 defaultColor;                            //!< AUTO: default cRGB olor for connectors; 4th value is alpha-transparency

  //! AUTO: default constructor with parameter initialization
  VSettingsConnectors()
  {
    show = true;
    showNumbers = false;
    showContact = false;
    defaultSize = 0.1f;
    contactPointsDefaultSize = 0.02f;
    defaultColor = Float4({0.2f,0.2f,1.f,1.f});
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual VSettingsConnectors* GetClone() const { return new VSettingsConnectors(*this); }
  
  //! AUTO: Read (Reference) access to: flag to decide, whether the connectors are shown
  const bool& GetShow() const { return show; }
  //! AUTO: Write (Reference) access to: flag to decide, whether the connectors are shown
  bool& GetShow() { return show; }

  //! AUTO: Read (Reference) access to: flag to decide, whether the connector(=object) number is shown
  const bool& GetShowNumbers() const { return showNumbers; }
  //! AUTO: Write (Reference) access to: flag to decide, whether the connector(=object) number is shown
  bool& GetShowNumbers() { return showNumbers; }

  //! AUTO: Read (Reference) access to: flag to decide, whether contact points, lines, etc. are shown
  const bool& GetShowContact() const { return showContact; }
  //! AUTO: Write (Reference) access to: flag to decide, whether contact points, lines, etc. are shown
  bool& GetShowContact() { return showContact; }

  //! AUTO: Read (Reference) access to: global connector size; if -1.f, connector size is relative to maxSceneSize
  const float& GetDefaultSize() const { return defaultSize; }
  //! AUTO: Write (Reference) access to: global connector size; if -1.f, connector size is relative to maxSceneSize
  float& GetDefaultSize() { return defaultSize; }

  //! AUTO: Read (Reference) access to: global contact points size; if -1.f, connector size is relative to maxSceneSize
  const float& GetContactPointsDefaultSize() const { return contactPointsDefaultSize; }
  //! AUTO: Write (Reference) access to: global contact points size; if -1.f, connector size is relative to maxSceneSize
  float& GetContactPointsDefaultSize() { return contactPointsDefaultSize; }

  //! AUTO: Read (Reference) access to: default cRGB olor for connectors; 4th value is alpha-transparency
  const Float4& GetDefaultColor() const { return defaultColor; }
  //! AUTO: Write (Reference) access to: default cRGB olor for connectors; 4th value is alpha-transparency
  Float4& GetDefaultColor() { return defaultColor; }
  //! AUTO: Set function (needed in pybind) for: default cRGB olor for connectors; 4th value is alpha-transparency
  void PySetDefaultColor(const std::vector<float>& defaultColorInit) { defaultColor = defaultColorInit; }
  //! AUTO: Read (Copy) access to: default cRGB olor for connectors; 4th value is alpha-transparency
  std::vector<float> PyGetDefaultColor() const { return (std::vector<float>)defaultColor; }

  virtual void Print(std::ostream& os) const
  {
    os << "VSettingsConnectors" << ":\n";
    os << "  show = " << show << "\n";
    os << "  showNumbers = " << showNumbers << "\n";
    os << "  showContact = " << showContact << "\n";
    os << "  defaultSize = " << defaultSize << "\n";
    os << "  contactPointsDefaultSize = " << contactPointsDefaultSize << "\n";
    os << "  defaultColor = " << defaultColor << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const VSettingsConnectors& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        VSettingsMarkers
* @brief        Visualization settings for markers.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class VSettingsMarkers // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  bool show;                                      //!< AUTO: flag to decide, whether the markers are shown
  bool showNumbers;                               //!< AUTO: flag to decide, whether the marker numbers are shown
  float defaultSize;                              //!< AUTO: global marker size; if -1.f, marker size is relative to maxSceneSize
  Float4 defaultColor;                            //!< AUTO: default cRGB olor for markers; 4th value is alpha-transparency

  //! AUTO: default constructor with parameter initialization
  VSettingsMarkers()
  {
    show = true;
    showNumbers = false;
    defaultSize = 0.1f;
    defaultColor = Float4({0.1f,0.5f,0.1f,1.f});
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual VSettingsMarkers* GetClone() const { return new VSettingsMarkers(*this); }
  
  //! AUTO: Read (Reference) access to: flag to decide, whether the markers are shown
  const bool& GetShow() const { return show; }
  //! AUTO: Write (Reference) access to: flag to decide, whether the markers are shown
  bool& GetShow() { return show; }

  //! AUTO: Read (Reference) access to: flag to decide, whether the marker numbers are shown
  const bool& GetShowNumbers() const { return showNumbers; }
  //! AUTO: Write (Reference) access to: flag to decide, whether the marker numbers are shown
  bool& GetShowNumbers() { return showNumbers; }

  //! AUTO: Read (Reference) access to: global marker size; if -1.f, marker size is relative to maxSceneSize
  const float& GetDefaultSize() const { return defaultSize; }
  //! AUTO: Write (Reference) access to: global marker size; if -1.f, marker size is relative to maxSceneSize
  float& GetDefaultSize() { return defaultSize; }

  //! AUTO: Read (Reference) access to: default cRGB olor for markers; 4th value is alpha-transparency
  const Float4& GetDefaultColor() const { return defaultColor; }
  //! AUTO: Write (Reference) access to: default cRGB olor for markers; 4th value is alpha-transparency
  Float4& GetDefaultColor() { return defaultColor; }
  //! AUTO: Set function (needed in pybind) for: default cRGB olor for markers; 4th value is alpha-transparency
  void PySetDefaultColor(const std::vector<float>& defaultColorInit) { defaultColor = defaultColorInit; }
  //! AUTO: Read (Copy) access to: default cRGB olor for markers; 4th value is alpha-transparency
  std::vector<float> PyGetDefaultColor() const { return (std::vector<float>)defaultColor; }

  virtual void Print(std::ostream& os) const
  {
    os << "VSettingsMarkers" << ":\n";
    os << "  show = " << show << "\n";
    os << "  showNumbers = " << showNumbers << "\n";
    os << "  defaultSize = " << defaultSize << "\n";
    os << "  defaultColor = " << defaultColor << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const VSettingsMarkers& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        VSettingsLoads
* @brief        Visualization settings for loads.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class VSettingsLoads // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  bool show;                                      //!< AUTO: flag to decide, whether the loads are shown
  bool showNumbers;                               //!< AUTO: flag to decide, whether the load numbers are shown
  float defaultSize;                              //!< AUTO: global load size; if -1.f, node size is relative to maxSceneSize
  bool fixedLoadSize;                             //!< AUTO: if true, the load is drawn with a fixed vector length in direction of the load vector, independently of the load size
  float loadSizeFactor;                           //!< AUTO: if fixedLoadSize=false, then this scaling factor is used to draw the load vector
  Float4 defaultColor;                            //!< AUTO: default cRGB olor for loads; 4th value is alpha-transparency

  //! AUTO: default constructor with parameter initialization
  VSettingsLoads()
  {
    show = true;
    showNumbers = false;
    defaultSize = 0.2f;
    fixedLoadSize = true;
    loadSizeFactor = 0.1f;
    defaultColor = Float4({0.7f,0.1f,0.1f,1.f});
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual VSettingsLoads* GetClone() const { return new VSettingsLoads(*this); }
  
  //! AUTO: Read (Reference) access to: flag to decide, whether the loads are shown
  const bool& GetShow() const { return show; }
  //! AUTO: Write (Reference) access to: flag to decide, whether the loads are shown
  bool& GetShow() { return show; }

  //! AUTO: Read (Reference) access to: flag to decide, whether the load numbers are shown
  const bool& GetShowNumbers() const { return showNumbers; }
  //! AUTO: Write (Reference) access to: flag to decide, whether the load numbers are shown
  bool& GetShowNumbers() { return showNumbers; }

  //! AUTO: Read (Reference) access to: global load size; if -1.f, node size is relative to maxSceneSize
  const float& GetDefaultSize() const { return defaultSize; }
  //! AUTO: Write (Reference) access to: global load size; if -1.f, node size is relative to maxSceneSize
  float& GetDefaultSize() { return defaultSize; }

  //! AUTO: Read (Reference) access to: if true, the load is drawn with a fixed vector length in direction of the load vector, independently of the load size
  const bool& GetFixedLoadSize() const { return fixedLoadSize; }
  //! AUTO: Write (Reference) access to: if true, the load is drawn with a fixed vector length in direction of the load vector, independently of the load size
  bool& GetFixedLoadSize() { return fixedLoadSize; }

  //! AUTO: Read (Reference) access to: if fixedLoadSize=false, then this scaling factor is used to draw the load vector
  const float& GetLoadSizeFactor() const { return loadSizeFactor; }
  //! AUTO: Write (Reference) access to: if fixedLoadSize=false, then this scaling factor is used to draw the load vector
  float& GetLoadSizeFactor() { return loadSizeFactor; }

  //! AUTO: Read (Reference) access to: default cRGB olor for loads; 4th value is alpha-transparency
  const Float4& GetDefaultColor() const { return defaultColor; }
  //! AUTO: Write (Reference) access to: default cRGB olor for loads; 4th value is alpha-transparency
  Float4& GetDefaultColor() { return defaultColor; }
  //! AUTO: Set function (needed in pybind) for: default cRGB olor for loads; 4th value is alpha-transparency
  void PySetDefaultColor(const std::vector<float>& defaultColorInit) { defaultColor = defaultColorInit; }
  //! AUTO: Read (Copy) access to: default cRGB olor for loads; 4th value is alpha-transparency
  std::vector<float> PyGetDefaultColor() const { return (std::vector<float>)defaultColor; }

  virtual void Print(std::ostream& os) const
  {
    os << "VSettingsLoads" << ":\n";
    os << "  show = " << show << "\n";
    os << "  showNumbers = " << showNumbers << "\n";
    os << "  defaultSize = " << defaultSize << "\n";
    os << "  fixedLoadSize = " << fixedLoadSize << "\n";
    os << "  loadSizeFactor = " << loadSizeFactor << "\n";
    os << "  defaultColor = " << defaultColor << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const VSettingsLoads& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        VisualizationSettings
* @brief        Settings for visualization
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class VisualizationSettings // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  VSettingsGeneral general;                       //!< AUTO: general visualization settings
  VSettingsWindow window;                         //!< AUTO: visualization window and interaction settings
  VSettingsOpenGL openGL;                         //!< AUTO: OpenGL rendering settings
  VSettingsContour contour;                       //!< AUTO: contour plot visualization settings
  VSettingsNodes nodes;                           //!< AUTO: node visualization settings
  VSettingsBodies bodies;                         //!< AUTO: body visualization settings
  VSettingsConnectors connectors;                 //!< AUTO: connector visualization settings
  VSettingsMarkers markers;                       //!< AUTO: marker visualization settings
  VSettingsLoads loads;                           //!< AUTO: load visualization settings


  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual VisualizationSettings* GetClone() const { return new VisualizationSettings(*this); }
  
  //! AUTO: Read (Reference) access to: general visualization settings
  const VSettingsGeneral& GetGeneral() const { return general; }
  //! AUTO: Write (Reference) access to: general visualization settings
  VSettingsGeneral& GetGeneral() { return general; }

  //! AUTO: Read (Reference) access to: visualization window and interaction settings
  const VSettingsWindow& GetWindow() const { return window; }
  //! AUTO: Write (Reference) access to: visualization window and interaction settings
  VSettingsWindow& GetWindow() { return window; }

  //! AUTO: Read (Reference) access to: OpenGL rendering settings
  const VSettingsOpenGL& GetOpenGL() const { return openGL; }
  //! AUTO: Write (Reference) access to: OpenGL rendering settings
  VSettingsOpenGL& GetOpenGL() { return openGL; }

  //! AUTO: Read (Reference) access to: contour plot visualization settings
  const VSettingsContour& GetContour() const { return contour; }
  //! AUTO: Write (Reference) access to: contour plot visualization settings
  VSettingsContour& GetContour() { return contour; }

  //! AUTO: Read (Reference) access to: node visualization settings
  const VSettingsNodes& GetNodes() const { return nodes; }
  //! AUTO: Write (Reference) access to: node visualization settings
  VSettingsNodes& GetNodes() { return nodes; }

  //! AUTO: Read (Reference) access to: body visualization settings
  const VSettingsBodies& GetBodies() const { return bodies; }
  //! AUTO: Write (Reference) access to: body visualization settings
  VSettingsBodies& GetBodies() { return bodies; }

  //! AUTO: Read (Reference) access to: connector visualization settings
  const VSettingsConnectors& GetConnectors() const { return connectors; }
  //! AUTO: Write (Reference) access to: connector visualization settings
  VSettingsConnectors& GetConnectors() { return connectors; }

  //! AUTO: Read (Reference) access to: marker visualization settings
  const VSettingsMarkers& GetMarkers() const { return markers; }
  //! AUTO: Write (Reference) access to: marker visualization settings
  VSettingsMarkers& GetMarkers() { return markers; }

  //! AUTO: Read (Reference) access to: load visualization settings
  const VSettingsLoads& GetLoads() const { return loads; }
  //! AUTO: Write (Reference) access to: load visualization settings
  VSettingsLoads& GetLoads() { return loads; }

  virtual void Print(std::ostream& os) const
  {
    os << "VisualizationSettings" << ":\n";
    os << "  general = " << general << "\n";
    os << "  window = " << window << "\n";
    os << "  openGL = " << openGL << "\n";
    os << "  contour = " << contour << "\n";
    os << "  nodes = " << nodes << "\n";
    os << "  bodies = " << bodies << "\n";
    os << "  connectors = " << connectors << "\n";
    os << "  markers = " << markers << "\n";
    os << "  loads = " << loads << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const VisualizationSettings& object)
  {
    object.Print(os);
    return os;
  }

};


