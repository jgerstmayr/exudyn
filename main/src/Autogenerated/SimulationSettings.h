/** ***********************************************************************************************
* @class        SolutionSettings
* @brief        General settings for exporting the solution (results) of a simulation.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class SolutionSettings // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  bool writeSolutionToFile;                       //!< AUTO: flag (true/false), which determines if (global) solution vector is written to file
  bool appendToFile;                              //!< AUTO: flag (true/false); if true, solution is always appended to file
  bool writeFileHeader;                           //!< AUTO: flag (true/false); if true, file header is written (turn off, e.g. for multiple runs of time integration)
  bool writeFileFooter;                           //!< AUTO: flag (true/false); if true, information at end of simulation is written: convergence, total solution time, statistics
  Real solutionWritePeriod;                       //!< AUTO: time span (period), determines how often the solution is written during a (dynamic) simulation
  bool exportVelocities;                          //!< AUTO: solution is written as displacements, velocities[, accelerations] [,algebraicCoordinates] [,DataCoordinates]
  bool exportAccelerations;                       //!< AUTO: solution is written as displacements, [velocities,] accelerations [,algebraicCoordinates] [,DataCoordinates]
  bool exportAlgebraicCoordinates;                //!< AUTO: solution is written as displacements, [velocities,] [accelerations,], algebraicCoordinates (=Lagrange multipliers) [,DataCoordinates]
  bool exportDataCoordinates;                     //!< AUTO: solution is written as displacements, [velocities,] [accelerations,] [,algebraicCoordinates (=Lagrange multipliers)] ,DataCoordinates
  std::string coordinatesSolutionFileName;        //!< AUTO: filename and (relative) path of solution file containing all coordinates versus time
  std::string solutionInformation;                //!< AUTO: special information added to header of solution file (e.g. parameters and settings, modes, ...)
  Index outputPrecision;                          //!< AUTO: precision for floating point numbers written to solution files

  //! AUTO: default constructor with parameter initialization
  SolutionSettings()
  {
    writeSolutionToFile = true;
    appendToFile = false;
    writeFileHeader = true;
    writeFileFooter = true;
    solutionWritePeriod = 0.01;
    exportVelocities = true;
    exportAccelerations = true;
    exportAlgebraicCoordinates = true;
    exportDataCoordinates = true;
    coordinatesSolutionFileName = "coordinatesSolution.txt";
    outputPrecision = 10;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual SolutionSettings* GetClone() const { return new SolutionSettings(*this); }
  
  //! AUTO: Read (Reference) access to: flag (true/false), which determines if (global) solution vector is written to file
  const bool& GetWriteSolutionToFile() const { return writeSolutionToFile; }
  //! AUTO: Write (Reference) access to: flag (true/false), which determines if (global) solution vector is written to file
  bool& GetWriteSolutionToFile() { return writeSolutionToFile; }

  //! AUTO: Read (Reference) access to: flag (true/false); if true, solution is always appended to file
  const bool& GetAppendToFile() const { return appendToFile; }
  //! AUTO: Write (Reference) access to: flag (true/false); if true, solution is always appended to file
  bool& GetAppendToFile() { return appendToFile; }

  //! AUTO: Read (Reference) access to: flag (true/false); if true, file header is written (turn off, e.g. for multiple runs of time integration)
  const bool& GetWriteFileHeader() const { return writeFileHeader; }
  //! AUTO: Write (Reference) access to: flag (true/false); if true, file header is written (turn off, e.g. for multiple runs of time integration)
  bool& GetWriteFileHeader() { return writeFileHeader; }

  //! AUTO: Read (Reference) access to: flag (true/false); if true, information at end of simulation is written: convergence, total solution time, statistics
  const bool& GetWriteFileFooter() const { return writeFileFooter; }
  //! AUTO: Write (Reference) access to: flag (true/false); if true, information at end of simulation is written: convergence, total solution time, statistics
  bool& GetWriteFileFooter() { return writeFileFooter; }

  //! AUTO: Read (Reference) access to: time span (period), determines how often the solution is written during a (dynamic) simulation
  const Real& GetSolutionWritePeriod() const { return solutionWritePeriod; }
  //! AUTO: Write (Reference) access to: time span (period), determines how often the solution is written during a (dynamic) simulation
  Real& GetSolutionWritePeriod() { return solutionWritePeriod; }

  //! AUTO: Read (Reference) access to: solution is written as displacements, velocities[, accelerations] [,algebraicCoordinates] [,DataCoordinates]
  const bool& GetExportVelocities() const { return exportVelocities; }
  //! AUTO: Write (Reference) access to: solution is written as displacements, velocities[, accelerations] [,algebraicCoordinates] [,DataCoordinates]
  bool& GetExportVelocities() { return exportVelocities; }

  //! AUTO: Read (Reference) access to: solution is written as displacements, [velocities,] accelerations [,algebraicCoordinates] [,DataCoordinates]
  const bool& GetExportAccelerations() const { return exportAccelerations; }
  //! AUTO: Write (Reference) access to: solution is written as displacements, [velocities,] accelerations [,algebraicCoordinates] [,DataCoordinates]
  bool& GetExportAccelerations() { return exportAccelerations; }

  //! AUTO: Read (Reference) access to: solution is written as displacements, [velocities,] [accelerations,], algebraicCoordinates (=Lagrange multipliers) [,DataCoordinates]
  const bool& GetExportAlgebraicCoordinates() const { return exportAlgebraicCoordinates; }
  //! AUTO: Write (Reference) access to: solution is written as displacements, [velocities,] [accelerations,], algebraicCoordinates (=Lagrange multipliers) [,DataCoordinates]
  bool& GetExportAlgebraicCoordinates() { return exportAlgebraicCoordinates; }

  //! AUTO: Read (Reference) access to: solution is written as displacements, [velocities,] [accelerations,] [,algebraicCoordinates (=Lagrange multipliers)] ,DataCoordinates
  const bool& GetExportDataCoordinates() const { return exportDataCoordinates; }
  //! AUTO: Write (Reference) access to: solution is written as displacements, [velocities,] [accelerations,] [,algebraicCoordinates (=Lagrange multipliers)] ,DataCoordinates
  bool& GetExportDataCoordinates() { return exportDataCoordinates; }

  //! AUTO: Read (Reference) access to: filename and (relative) path of solution file containing all coordinates versus time
  const std::string& GetCoordinatesSolutionFileName() const { return coordinatesSolutionFileName; }
  //! AUTO: Write (Reference) access to: filename and (relative) path of solution file containing all coordinates versus time
  std::string& GetCoordinatesSolutionFileName() { return coordinatesSolutionFileName; }

  //! AUTO: Read (Reference) access to: special information added to header of solution file (e.g. parameters and settings, modes, ...)
  const std::string& GetSolutionInformation() const { return solutionInformation; }
  //! AUTO: Write (Reference) access to: special information added to header of solution file (e.g. parameters and settings, modes, ...)
  std::string& GetSolutionInformation() { return solutionInformation; }

  //! AUTO: Read (Reference) access to: precision for floating point numbers written to solution files
  const Index& GetOutputPrecision() const { return outputPrecision; }
  //! AUTO: Write (Reference) access to: precision for floating point numbers written to solution files
  Index& GetOutputPrecision() { return outputPrecision; }

  virtual void Print(std::ostream& os) const
  {
    os << "SolutionSettings" << ":\n";
    os << "  writeSolutionToFile = " << writeSolutionToFile << "\n";
    os << "  appendToFile = " << appendToFile << "\n";
    os << "  writeFileHeader = " << writeFileHeader << "\n";
    os << "  writeFileFooter = " << writeFileFooter << "\n";
    os << "  solutionWritePeriod = " << solutionWritePeriod << "\n";
    os << "  exportVelocities = " << exportVelocities << "\n";
    os << "  exportAccelerations = " << exportAccelerations << "\n";
    os << "  exportAlgebraicCoordinates = " << exportAlgebraicCoordinates << "\n";
    os << "  exportDataCoordinates = " << exportDataCoordinates << "\n";
    os << "  coordinatesSolutionFileName = " << coordinatesSolutionFileName << "\n";
    os << "  solutionInformation = " << solutionInformation << "\n";
    os << "  outputPrecision = " << outputPrecision << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const SolutionSettings& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        NumericalDifferentiation
* @brief        Settings for numerical differentiation of a function (needed for computation of numerical jacobian e.g. in implizit integration); HOTINT1: relativeEpsilon * Maximum(minimumCoordinateSize, fabs(x(i))).
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class NumericalDifferentiation // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  Real relativeEpsilon;                           //!< AUTO: relative differentiation parameter epsilon; the numerical differentiation parameter \f$\varepsilon\f$ follows from the formula (\f$\varepsilon = \varepsilon_\mathrm{relative}*max(q_{min}, |q_i + [q^{Ref}_i]|)\f$, with \f$\varepsilon_\mathrm{relative}\f$=relativeEpsilon, \f$q_{min} = \f$minimumCoordinateSize, \f$q_i\f$ is the current coordinate which is differentiated, and \f$qRef_i\f$ is the reference coordinate of the current coordinate
  Real minimumCoordinateSize;                     //!< AUTO: minimum size of coordinates in relative differentiation parameter
  bool doSystemWideDifferentiation;               //!< AUTO: true: system wide differentiation (e.g. all ODE2 equations w.r.t. all ODE2 coordinates); false: only local (object) differentiation
  bool addReferenceCoordinatesToEpsilon;          //!< AUTO: true: for the size estimation of the differentiation parameter, the reference coordinate \f$q^{Ref}_i\f$ is added to ODE2 coordinates --> see; false: only the current coordinate is used for size estimation of the differentiation parameter

  //! AUTO: default constructor with parameter initialization
  NumericalDifferentiation()
  {
    relativeEpsilon = 1e-7;
    minimumCoordinateSize = 1e-2;
    doSystemWideDifferentiation = false;
    addReferenceCoordinatesToEpsilon = false;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual NumericalDifferentiation* GetClone() const { return new NumericalDifferentiation(*this); }
  
  //! AUTO: Read (Reference) access to: relative differentiation parameter epsilon; the numerical differentiation parameter \f$\varepsilon\f$ follows from the formula (\f$\varepsilon = \varepsilon_\mathrm{relative}*max(q_{min}, |q_i + [q^{Ref}_i]|)\f$, with \f$\varepsilon_\mathrm{relative}\f$=relativeEpsilon, \f$q_{min} = \f$minimumCoordinateSize, \f$q_i\f$ is the current coordinate which is differentiated, and \f$qRef_i\f$ is the reference coordinate of the current coordinate
  const Real& GetRelativeEpsilon() const { return relativeEpsilon; }
  //! AUTO: Write (Reference) access to: relative differentiation parameter epsilon; the numerical differentiation parameter \f$\varepsilon\f$ follows from the formula (\f$\varepsilon = \varepsilon_\mathrm{relative}*max(q_{min}, |q_i + [q^{Ref}_i]|)\f$, with \f$\varepsilon_\mathrm{relative}\f$=relativeEpsilon, \f$q_{min} = \f$minimumCoordinateSize, \f$q_i\f$ is the current coordinate which is differentiated, and \f$qRef_i\f$ is the reference coordinate of the current coordinate
  Real& GetRelativeEpsilon() { return relativeEpsilon; }

  //! AUTO: Read (Reference) access to: minimum size of coordinates in relative differentiation parameter
  const Real& GetMinimumCoordinateSize() const { return minimumCoordinateSize; }
  //! AUTO: Write (Reference) access to: minimum size of coordinates in relative differentiation parameter
  Real& GetMinimumCoordinateSize() { return minimumCoordinateSize; }

  //! AUTO: Read (Reference) access to: true: system wide differentiation (e.g. all ODE2 equations w.r.t. all ODE2 coordinates); false: only local (object) differentiation
  const bool& GetDoSystemWideDifferentiation() const { return doSystemWideDifferentiation; }
  //! AUTO: Write (Reference) access to: true: system wide differentiation (e.g. all ODE2 equations w.r.t. all ODE2 coordinates); false: only local (object) differentiation
  bool& GetDoSystemWideDifferentiation() { return doSystemWideDifferentiation; }

  //! AUTO: Read (Reference) access to: true: for the size estimation of the differentiation parameter, the reference coordinate \f$q^{Ref}_i\f$ is added to ODE2 coordinates --> see; false: only the current coordinate is used for size estimation of the differentiation parameter
  const bool& GetAddReferenceCoordinatesToEpsilon() const { return addReferenceCoordinatesToEpsilon; }
  //! AUTO: Write (Reference) access to: true: for the size estimation of the differentiation parameter, the reference coordinate \f$q^{Ref}_i\f$ is added to ODE2 coordinates --> see; false: only the current coordinate is used for size estimation of the differentiation parameter
  bool& GetAddReferenceCoordinatesToEpsilon() { return addReferenceCoordinatesToEpsilon; }

  virtual void Print(std::ostream& os) const
  {
    os << "NumericalDifferentiation" << ":\n";
    os << "  relativeEpsilon = " << relativeEpsilon << "\n";
    os << "  minimumCoordinateSize = " << minimumCoordinateSize << "\n";
    os << "  doSystemWideDifferentiation = " << doSystemWideDifferentiation << "\n";
    os << "  addReferenceCoordinatesToEpsilon = " << addReferenceCoordinatesToEpsilon << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const NumericalDifferentiation& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        Newton
* @brief        Settings for Newton method used in static or dynamic simulation.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class Newton // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  NumericalDifferentiation numericalDifferentiation;//!< AUTO: numerical differentiation parameters for numerical jacobian (e.g. Newton in static solver or implicit time integration)
  bool useNumericalDifferentiation;               //!< AUTO: flag (true/false); false = perform direct computation of jacobian, true = use numerical differentiation for jacobian
  bool useNewtonSolver;                           //!< AUTO: flag (true/false); false = linear computation, true = use Newton solver for nonlinear solution
  Real relativeTolerance;                         //!< AUTO: relative tolerance of residual for Newton (general goal of Newton is to decrease the residual by this factor)
  Real absoluteTolerance;                         //!< AUTO: absolute tolerance of residual for Newton (needed e.g. if residual is fulfilled right at beginning); condition: sqrt(q*q)/numberOfCoordinates <= absoluteTolerance
  Real modifiedNewtonContractivity;               //!< AUTO: maximum contractivity (=reduction of error in every Newton iteration) accepted by modified Newton; if contractivity is greater, a Jacobian update is computed
  Index useModifiedNewton;                        //!< AUTO: true: Jacobian only at first step; no Jacobian updates per step; false: Jacobian computed in every step
  Index maxIterations;                            //!< AUTO: maximum number of iterations (including modified + restart Newton steps); after that iterations, the static/dynamic solver stops with error
  Index maxModifiedNewtonIterations;              //!< AUTO: maximum number of iterations for modified Newton (without Jacobian update); after that number of iterations, the modified Newton method gets a jacobian update and is further iterated
  Index maxModifiedNewtonRestartIterations;       //!< AUTO: maximum number of iterations for modified Newton after aJacobian update; after that number of iterations, the full Newton method is started for this step
  Real maximumSolutionNorm;                       //!< AUTO: this is the maximum allowed value for solutionU.L2NormSquared() which is the square of the square norm (value=\f$u_1^2\f$+\f$u_2^2\f$+...), and solutionV/A...; if the norm of solution vectors are larger, Newton method is stopped; the default value is chosen such that it would still work for single precision numbers (float)
  Index maxDiscontinuousIterations;               //!< AUTO: maximum number of discontinuous (post Newton) iterations
  Real discontinuousIterationTolerance;           //!< AUTO: absolute tolerance for discontinuous (post Newton) iterations; the errors represent absolute residuals and can be quite high

  //! AUTO: default constructor with parameter initialization
  Newton()
  {
    useNumericalDifferentiation = false;
    useNewtonSolver = true;
    relativeTolerance = 1e-8;
    absoluteTolerance = 1e-10;
    modifiedNewtonContractivity = 0.5;
    useModifiedNewton = false;
    maxIterations = 20;
    maxModifiedNewtonIterations = 10;
    maxModifiedNewtonRestartIterations = 5;
    maximumSolutionNorm = 1e38;
    maxDiscontinuousIterations = 5;
    discontinuousIterationTolerance = 1;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual Newton* GetClone() const { return new Newton(*this); }
  
  //! AUTO: Read (Reference) access to: numerical differentiation parameters for numerical jacobian (e.g. Newton in static solver or implicit time integration)
  const NumericalDifferentiation& GetNumericalDifferentiation() const { return numericalDifferentiation; }
  //! AUTO: Write (Reference) access to: numerical differentiation parameters for numerical jacobian (e.g. Newton in static solver or implicit time integration)
  NumericalDifferentiation& GetNumericalDifferentiation() { return numericalDifferentiation; }

  //! AUTO: Read (Reference) access to: flag (true/false); false = perform direct computation of jacobian, true = use numerical differentiation for jacobian
  const bool& GetUseNumericalDifferentiation() const { return useNumericalDifferentiation; }
  //! AUTO: Write (Reference) access to: flag (true/false); false = perform direct computation of jacobian, true = use numerical differentiation for jacobian
  bool& GetUseNumericalDifferentiation() { return useNumericalDifferentiation; }

  //! AUTO: Read (Reference) access to: flag (true/false); false = linear computation, true = use Newton solver for nonlinear solution
  const bool& GetUseNewtonSolver() const { return useNewtonSolver; }
  //! AUTO: Write (Reference) access to: flag (true/false); false = linear computation, true = use Newton solver for nonlinear solution
  bool& GetUseNewtonSolver() { return useNewtonSolver; }

  //! AUTO: Read (Reference) access to: relative tolerance of residual for Newton (general goal of Newton is to decrease the residual by this factor)
  const Real& GetRelativeTolerance() const { return relativeTolerance; }
  //! AUTO: Write (Reference) access to: relative tolerance of residual for Newton (general goal of Newton is to decrease the residual by this factor)
  Real& GetRelativeTolerance() { return relativeTolerance; }

  //! AUTO: Read (Reference) access to: absolute tolerance of residual for Newton (needed e.g. if residual is fulfilled right at beginning); condition: sqrt(q*q)/numberOfCoordinates <= absoluteTolerance
  const Real& GetAbsoluteTolerance() const { return absoluteTolerance; }
  //! AUTO: Write (Reference) access to: absolute tolerance of residual for Newton (needed e.g. if residual is fulfilled right at beginning); condition: sqrt(q*q)/numberOfCoordinates <= absoluteTolerance
  Real& GetAbsoluteTolerance() { return absoluteTolerance; }

  //! AUTO: Read (Reference) access to: maximum contractivity (=reduction of error in every Newton iteration) accepted by modified Newton; if contractivity is greater, a Jacobian update is computed
  const Real& GetModifiedNewtonContractivity() const { return modifiedNewtonContractivity; }
  //! AUTO: Write (Reference) access to: maximum contractivity (=reduction of error in every Newton iteration) accepted by modified Newton; if contractivity is greater, a Jacobian update is computed
  Real& GetModifiedNewtonContractivity() { return modifiedNewtonContractivity; }

  //! AUTO: Read (Reference) access to: true: Jacobian only at first step; no Jacobian updates per step; false: Jacobian computed in every step
  const Index& GetUseModifiedNewton() const { return useModifiedNewton; }
  //! AUTO: Write (Reference) access to: true: Jacobian only at first step; no Jacobian updates per step; false: Jacobian computed in every step
  Index& GetUseModifiedNewton() { return useModifiedNewton; }

  //! AUTO: Read (Reference) access to: maximum number of iterations (including modified + restart Newton steps); after that iterations, the static/dynamic solver stops with error
  const Index& GetMaxIterations() const { return maxIterations; }
  //! AUTO: Write (Reference) access to: maximum number of iterations (including modified + restart Newton steps); after that iterations, the static/dynamic solver stops with error
  Index& GetMaxIterations() { return maxIterations; }

  //! AUTO: Read (Reference) access to: maximum number of iterations for modified Newton (without Jacobian update); after that number of iterations, the modified Newton method gets a jacobian update and is further iterated
  const Index& GetMaxModifiedNewtonIterations() const { return maxModifiedNewtonIterations; }
  //! AUTO: Write (Reference) access to: maximum number of iterations for modified Newton (without Jacobian update); after that number of iterations, the modified Newton method gets a jacobian update and is further iterated
  Index& GetMaxModifiedNewtonIterations() { return maxModifiedNewtonIterations; }

  //! AUTO: Read (Reference) access to: maximum number of iterations for modified Newton after aJacobian update; after that number of iterations, the full Newton method is started for this step
  const Index& GetMaxModifiedNewtonRestartIterations() const { return maxModifiedNewtonRestartIterations; }
  //! AUTO: Write (Reference) access to: maximum number of iterations for modified Newton after aJacobian update; after that number of iterations, the full Newton method is started for this step
  Index& GetMaxModifiedNewtonRestartIterations() { return maxModifiedNewtonRestartIterations; }

  //! AUTO: Read (Reference) access to: this is the maximum allowed value for solutionU.L2NormSquared() which is the square of the square norm (value=\f$u_1^2\f$+\f$u_2^2\f$+...), and solutionV/A...; if the norm of solution vectors are larger, Newton method is stopped; the default value is chosen such that it would still work for single precision numbers (float)
  const Real& GetMaximumSolutionNorm() const { return maximumSolutionNorm; }
  //! AUTO: Write (Reference) access to: this is the maximum allowed value for solutionU.L2NormSquared() which is the square of the square norm (value=\f$u_1^2\f$+\f$u_2^2\f$+...), and solutionV/A...; if the norm of solution vectors are larger, Newton method is stopped; the default value is chosen such that it would still work for single precision numbers (float)
  Real& GetMaximumSolutionNorm() { return maximumSolutionNorm; }

  //! AUTO: Read (Reference) access to: maximum number of discontinuous (post Newton) iterations
  const Index& GetMaxDiscontinuousIterations() const { return maxDiscontinuousIterations; }
  //! AUTO: Write (Reference) access to: maximum number of discontinuous (post Newton) iterations
  Index& GetMaxDiscontinuousIterations() { return maxDiscontinuousIterations; }

  //! AUTO: Read (Reference) access to: absolute tolerance for discontinuous (post Newton) iterations; the errors represent absolute residuals and can be quite high
  const Real& GetDiscontinuousIterationTolerance() const { return discontinuousIterationTolerance; }
  //! AUTO: Write (Reference) access to: absolute tolerance for discontinuous (post Newton) iterations; the errors represent absolute residuals and can be quite high
  Real& GetDiscontinuousIterationTolerance() { return discontinuousIterationTolerance; }

  virtual void Print(std::ostream& os) const
  {
    os << "Newton" << ":\n";
    os << "  numericalDifferentiation = " << numericalDifferentiation << "\n";
    os << "  useNumericalDifferentiation = " << useNumericalDifferentiation << "\n";
    os << "  useNewtonSolver = " << useNewtonSolver << "\n";
    os << "  relativeTolerance = " << relativeTolerance << "\n";
    os << "  absoluteTolerance = " << absoluteTolerance << "\n";
    os << "  modifiedNewtonContractivity = " << modifiedNewtonContractivity << "\n";
    os << "  useModifiedNewton = " << useModifiedNewton << "\n";
    os << "  maxIterations = " << maxIterations << "\n";
    os << "  maxModifiedNewtonIterations = " << maxModifiedNewtonIterations << "\n";
    os << "  maxModifiedNewtonRestartIterations = " << maxModifiedNewtonRestartIterations << "\n";
    os << "  maximumSolutionNorm = " << maximumSolutionNorm << "\n";
    os << "  maxDiscontinuousIterations = " << maxDiscontinuousIterations << "\n";
    os << "  discontinuousIterationTolerance = " << discontinuousIterationTolerance << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const Newton& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        GeneralizedAlpha
* @brief        Settings for generalized-alpha, implicit trapezoidal or Newmark time integration methods.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class GeneralizedAlpha // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  Real newmarkBeta;                               //!< AUTO: value beta for Newmark method; default value beta = \f$\frac 1 4\f$ corresponds to (undamped) trapezoidal rule
  Real newmarkGamma;                              //!< AUTO: value gamma for Newmark method; default value gamma = \f$\frac 1 2\f$ corresponds to (undamped) trapezoidal rule
  bool useIndex2Constraints;                      //!< AUTO: set useIndex2Constraints = true in order to use index2 (velocity level constraints) formulation
  bool useNewmark;                                //!< AUTO: if true, use Newmark method with beta and gamma instead of generalized-Alpha
  Real spectralRadius;                            //!< AUTO: spectral radius for Generalized-alpha solver; set this value to 1 for no damping or to 0 < spectralRadius < 1 for damping of high-frequency dynamics; for position-level constraints (index 3), spectralRadius must be < 1

  //! AUTO: default constructor with parameter initialization
  GeneralizedAlpha()
  {
    newmarkBeta = 0.25;
    newmarkGamma = 0.5;
    useIndex2Constraints = false;
    useNewmark = false;
    spectralRadius = 0.9;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual GeneralizedAlpha* GetClone() const { return new GeneralizedAlpha(*this); }
  
  //! AUTO: Read (Reference) access to: value beta for Newmark method; default value beta = \f$\frac 1 4\f$ corresponds to (undamped) trapezoidal rule
  const Real& GetNewmarkBeta() const { return newmarkBeta; }
  //! AUTO: Write (Reference) access to: value beta for Newmark method; default value beta = \f$\frac 1 4\f$ corresponds to (undamped) trapezoidal rule
  Real& GetNewmarkBeta() { return newmarkBeta; }

  //! AUTO: Read (Reference) access to: value gamma for Newmark method; default value gamma = \f$\frac 1 2\f$ corresponds to (undamped) trapezoidal rule
  const Real& GetNewmarkGamma() const { return newmarkGamma; }
  //! AUTO: Write (Reference) access to: value gamma for Newmark method; default value gamma = \f$\frac 1 2\f$ corresponds to (undamped) trapezoidal rule
  Real& GetNewmarkGamma() { return newmarkGamma; }

  //! AUTO: Read (Reference) access to: set useIndex2Constraints = true in order to use index2 (velocity level constraints) formulation
  const bool& GetUseIndex2Constraints() const { return useIndex2Constraints; }
  //! AUTO: Write (Reference) access to: set useIndex2Constraints = true in order to use index2 (velocity level constraints) formulation
  bool& GetUseIndex2Constraints() { return useIndex2Constraints; }

  //! AUTO: Read (Reference) access to: if true, use Newmark method with beta and gamma instead of generalized-Alpha
  const bool& GetUseNewmark() const { return useNewmark; }
  //! AUTO: Write (Reference) access to: if true, use Newmark method with beta and gamma instead of generalized-Alpha
  bool& GetUseNewmark() { return useNewmark; }

  //! AUTO: Read (Reference) access to: spectral radius for Generalized-alpha solver; set this value to 1 for no damping or to 0 < spectralRadius < 1 for damping of high-frequency dynamics; for position-level constraints (index 3), spectralRadius must be < 1
  const Real& GetSpectralRadius() const { return spectralRadius; }
  //! AUTO: Write (Reference) access to: spectral radius for Generalized-alpha solver; set this value to 1 for no damping or to 0 < spectralRadius < 1 for damping of high-frequency dynamics; for position-level constraints (index 3), spectralRadius must be < 1
  Real& GetSpectralRadius() { return spectralRadius; }

  virtual void Print(std::ostream& os) const
  {
    os << "GeneralizedAlpha" << ":\n";
    os << "  newmarkBeta = " << newmarkBeta << "\n";
    os << "  newmarkGamma = " << newmarkGamma << "\n";
    os << "  useIndex2Constraints = " << useIndex2Constraints << "\n";
    os << "  useNewmark = " << useNewmark << "\n";
    os << "  spectralRadius = " << spectralRadius << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const GeneralizedAlpha& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        TimeIntegration
* @brief        General parameters used in time integration; specific parameters are provided in the according solver settings, e.g. for generalizedAlpha.
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class TimeIntegration // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  Real startTime;                                 //!< AUTO: start time of time integration (usually set to zero)
  Real endTime;                                   //!< AUTO: end time of time integration
  Index numberOfSteps;                            //!< AUTO: number of steps in time integration; stepsize is computed from (endTime-startTime)/numberOfSteps
  Index verboseMode;                              //!< AUTO: 0 ... no output, 1 ... show short step information every 2 seconds (error), 2 ... show every step information, 3 ... show also solution vector, 4 ... show also mass matrix and jacobian (implicit methods), 5 ... show also Jacobian inverse (implicit methods)
  bool pauseAfterEachStep;                        //!< AUTO: pause after every time step (user press SPACE)
  Newton newton;                                  //!< AUTO: parameters for Newton method; used for implicit time integration methods only
  GeneralizedAlpha generalizedAlpha;              //!< AUTO: parameters for generalized-alpha, implicit trapezoidal rule or Newmark (options only apply for these methods)
  std::string preStepPyExecute;                   //!< AUTO: Python code to be executed prior to every step and after last step, e.g. for postprocessing

  //! AUTO: default constructor with parameter initialization
  TimeIntegration()
  {
    startTime = 0;
    endTime = 1;
    numberOfSteps = 100;
    verboseMode = 0;
    pauseAfterEachStep = false;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual TimeIntegration* GetClone() const { return new TimeIntegration(*this); }
  
  //! AUTO: Read (Reference) access to: start time of time integration (usually set to zero)
  const Real& GetStartTime() const { return startTime; }
  //! AUTO: Write (Reference) access to: start time of time integration (usually set to zero)
  Real& GetStartTime() { return startTime; }

  //! AUTO: Read (Reference) access to: end time of time integration
  const Real& GetEndTime() const { return endTime; }
  //! AUTO: Write (Reference) access to: end time of time integration
  Real& GetEndTime() { return endTime; }

  //! AUTO: Read (Reference) access to: number of steps in time integration; stepsize is computed from (endTime-startTime)/numberOfSteps
  const Index& GetNumberOfSteps() const { return numberOfSteps; }
  //! AUTO: Write (Reference) access to: number of steps in time integration; stepsize is computed from (endTime-startTime)/numberOfSteps
  Index& GetNumberOfSteps() { return numberOfSteps; }

  //! AUTO: Read (Reference) access to: 0 ... no output, 1 ... show short step information every 2 seconds (error), 2 ... show every step information, 3 ... show also solution vector, 4 ... show also mass matrix and jacobian (implicit methods), 5 ... show also Jacobian inverse (implicit methods)
  const Index& GetVerboseMode() const { return verboseMode; }
  //! AUTO: Write (Reference) access to: 0 ... no output, 1 ... show short step information every 2 seconds (error), 2 ... show every step information, 3 ... show also solution vector, 4 ... show also mass matrix and jacobian (implicit methods), 5 ... show also Jacobian inverse (implicit methods)
  Index& GetVerboseMode() { return verboseMode; }

  //! AUTO: Read (Reference) access to: pause after every time step (user press SPACE)
  const bool& GetPauseAfterEachStep() const { return pauseAfterEachStep; }
  //! AUTO: Write (Reference) access to: pause after every time step (user press SPACE)
  bool& GetPauseAfterEachStep() { return pauseAfterEachStep; }

  //! AUTO: Read (Reference) access to: parameters for Newton method; used for implicit time integration methods only
  const Newton& GetNewton() const { return newton; }
  //! AUTO: Write (Reference) access to: parameters for Newton method; used for implicit time integration methods only
  Newton& GetNewton() { return newton; }

  //! AUTO: Read (Reference) access to: parameters for generalized-alpha, implicit trapezoidal rule or Newmark (options only apply for these methods)
  const GeneralizedAlpha& GetGeneralizedAlpha() const { return generalizedAlpha; }
  //! AUTO: Write (Reference) access to: parameters for generalized-alpha, implicit trapezoidal rule or Newmark (options only apply for these methods)
  GeneralizedAlpha& GetGeneralizedAlpha() { return generalizedAlpha; }

  //! AUTO: Read (Reference) access to: Python code to be executed prior to every step and after last step, e.g. for postprocessing
  const std::string& GetPreStepPyExecute() const { return preStepPyExecute; }
  //! AUTO: Write (Reference) access to: Python code to be executed prior to every step and after last step, e.g. for postprocessing
  std::string& GetPreStepPyExecute() { return preStepPyExecute; }

  virtual void Print(std::ostream& os) const
  {
    os << "TimeIntegration" << ":\n";
    os << "  startTime = " << startTime << "\n";
    os << "  endTime = " << endTime << "\n";
    os << "  numberOfSteps = " << numberOfSteps << "\n";
    os << "  verboseMode = " << verboseMode << "\n";
    os << "  pauseAfterEachStep = " << pauseAfterEachStep << "\n";
    os << "  newton = " << newton << "\n";
    os << "  generalizedAlpha = " << generalizedAlpha << "\n";
    os << "  preStepPyExecute = " << preStepPyExecute << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const TimeIntegration& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        StaticSolver
* @brief        Settings for static solver linear or nonlinear (Newton).
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class StaticSolver // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  Newton newton;                                  //!< AUTO: parameters for Newton method (e.g. in static solver or time integration)
  Index numberOfLoadSteps;                        //!< AUTO: number of load steps; if numberOfLoadSteps=1, no load steps are used and full forces are applied at once
  Real loadStepDuration;                          //!< AUTO: quasi-time for all load steps (added to current time in load steps)
  Real loadStepStart;                             //!< AUTO: a quasi time, which can be used for the output (first column) as well as for time-dependent forces; quasi-time is increased in every step i by loadStepDuration/numberOfLoadSteps; loadStepTime = loadStepStart + i*loadStepDuration/numberOfLoadSteps, but loadStepStart untouched ==> increment by user
  bool loadStepGeometric;                         //!< AUTO: if loadStepGeometric=false, the load steps are incremental (arithmetic series, e.g. 0.1,0.2,0.3,...); if true, the load steps are increased in a geometric series, e.g. for \f$n=8\f$ numberOfLoadSteps and \f$d = 1000\f$ loadStepGeometricRange, it follows: \f$1000^{1/8}/1000=0.00237\f$, \f$1000^{2/8}/1000=0.00562\f$, \f$1000^{3/8}/1000=0.0133\f$, ..., \f$1000^{7/8}/1000=0.422\f$, \f$1000^{8/8}/1000=1\f$
  Real loadStepGeometricRange;                    //!< AUTO: if loadStepGeometric=true, the load steps are increased in a geometric series, see loadStepGeometric
  Real stabilizerODE2term;                        //!< AUTO: add mass-proportional stabilizer term in ODE2 part of jacobian for stabilization (scaled ), e.g. of badly conditioned problems; the diagnoal terms are scaled with \f$stabilizer = (1-loadStepFactor^2)\f$, and go to zero at the end of all load steps: \f$loadStepFactor=1\f$ -> \f$stabilizer = 0\f$
  Index verboseMode;                              //!< AUTO: 0 ... no output, 1 ... show errors and load steps, 2 ... show short Newton step information (error), 3 ... show also solution vector, 4 ... show also jacobian, 5 ... show also Jacobian inverse
  bool pauseAfterEachStep;                        //!< AUTO: pause after every load step (user press SPACE)

  //! AUTO: default constructor with parameter initialization
  StaticSolver()
  {
    numberOfLoadSteps = 1;
    loadStepDuration = 1;
    loadStepStart = 0;
    loadStepGeometric = false;
    loadStepGeometricRange = 1000;
    stabilizerODE2term = 0;
    verboseMode = 1;
    pauseAfterEachStep = false;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual StaticSolver* GetClone() const { return new StaticSolver(*this); }
  
  //! AUTO: Read (Reference) access to: parameters for Newton method (e.g. in static solver or time integration)
  const Newton& GetNewton() const { return newton; }
  //! AUTO: Write (Reference) access to: parameters for Newton method (e.g. in static solver or time integration)
  Newton& GetNewton() { return newton; }

  //! AUTO: Read (Reference) access to: number of load steps; if numberOfLoadSteps=1, no load steps are used and full forces are applied at once
  const Index& GetNumberOfLoadSteps() const { return numberOfLoadSteps; }
  //! AUTO: Write (Reference) access to: number of load steps; if numberOfLoadSteps=1, no load steps are used and full forces are applied at once
  Index& GetNumberOfLoadSteps() { return numberOfLoadSteps; }

  //! AUTO: Read (Reference) access to: quasi-time for all load steps (added to current time in load steps)
  const Real& GetLoadStepDuration() const { return loadStepDuration; }
  //! AUTO: Write (Reference) access to: quasi-time for all load steps (added to current time in load steps)
  Real& GetLoadStepDuration() { return loadStepDuration; }

  //! AUTO: Read (Reference) access to: a quasi time, which can be used for the output (first column) as well as for time-dependent forces; quasi-time is increased in every step i by loadStepDuration/numberOfLoadSteps; loadStepTime = loadStepStart + i*loadStepDuration/numberOfLoadSteps, but loadStepStart untouched ==> increment by user
  const Real& GetLoadStepStart() const { return loadStepStart; }
  //! AUTO: Write (Reference) access to: a quasi time, which can be used for the output (first column) as well as for time-dependent forces; quasi-time is increased in every step i by loadStepDuration/numberOfLoadSteps; loadStepTime = loadStepStart + i*loadStepDuration/numberOfLoadSteps, but loadStepStart untouched ==> increment by user
  Real& GetLoadStepStart() { return loadStepStart; }

  //! AUTO: Read (Reference) access to: if loadStepGeometric=false, the load steps are incremental (arithmetic series, e.g. 0.1,0.2,0.3,...); if true, the load steps are increased in a geometric series, e.g. for \f$n=8\f$ numberOfLoadSteps and \f$d = 1000\f$ loadStepGeometricRange, it follows: \f$1000^{1/8}/1000=0.00237\f$, \f$1000^{2/8}/1000=0.00562\f$, \f$1000^{3/8}/1000=0.0133\f$, ..., \f$1000^{7/8}/1000=0.422\f$, \f$1000^{8/8}/1000=1\f$
  const bool& GetLoadStepGeometric() const { return loadStepGeometric; }
  //! AUTO: Write (Reference) access to: if loadStepGeometric=false, the load steps are incremental (arithmetic series, e.g. 0.1,0.2,0.3,...); if true, the load steps are increased in a geometric series, e.g. for \f$n=8\f$ numberOfLoadSteps and \f$d = 1000\f$ loadStepGeometricRange, it follows: \f$1000^{1/8}/1000=0.00237\f$, \f$1000^{2/8}/1000=0.00562\f$, \f$1000^{3/8}/1000=0.0133\f$, ..., \f$1000^{7/8}/1000=0.422\f$, \f$1000^{8/8}/1000=1\f$
  bool& GetLoadStepGeometric() { return loadStepGeometric; }

  //! AUTO: Read (Reference) access to: if loadStepGeometric=true, the load steps are increased in a geometric series, see loadStepGeometric
  const Real& GetLoadStepGeometricRange() const { return loadStepGeometricRange; }
  //! AUTO: Write (Reference) access to: if loadStepGeometric=true, the load steps are increased in a geometric series, see loadStepGeometric
  Real& GetLoadStepGeometricRange() { return loadStepGeometricRange; }

  //! AUTO: Read (Reference) access to: add mass-proportional stabilizer term in ODE2 part of jacobian for stabilization (scaled ), e.g. of badly conditioned problems; the diagnoal terms are scaled with \f$stabilizer = (1-loadStepFactor^2)\f$, and go to zero at the end of all load steps: \f$loadStepFactor=1\f$ -> \f$stabilizer = 0\f$
  const Real& GetStabilizerODE2term() const { return stabilizerODE2term; }
  //! AUTO: Write (Reference) access to: add mass-proportional stabilizer term in ODE2 part of jacobian for stabilization (scaled ), e.g. of badly conditioned problems; the diagnoal terms are scaled with \f$stabilizer = (1-loadStepFactor^2)\f$, and go to zero at the end of all load steps: \f$loadStepFactor=1\f$ -> \f$stabilizer = 0\f$
  Real& GetStabilizerODE2term() { return stabilizerODE2term; }

  //! AUTO: Read (Reference) access to: 0 ... no output, 1 ... show errors and load steps, 2 ... show short Newton step information (error), 3 ... show also solution vector, 4 ... show also jacobian, 5 ... show also Jacobian inverse
  const Index& GetVerboseMode() const { return verboseMode; }
  //! AUTO: Write (Reference) access to: 0 ... no output, 1 ... show errors and load steps, 2 ... show short Newton step information (error), 3 ... show also solution vector, 4 ... show also jacobian, 5 ... show also Jacobian inverse
  Index& GetVerboseMode() { return verboseMode; }

  //! AUTO: Read (Reference) access to: pause after every load step (user press SPACE)
  const bool& GetPauseAfterEachStep() const { return pauseAfterEachStep; }
  //! AUTO: Write (Reference) access to: pause after every load step (user press SPACE)
  bool& GetPauseAfterEachStep() { return pauseAfterEachStep; }

  virtual void Print(std::ostream& os) const
  {
    os << "StaticSolver" << ":\n";
    os << "  newton = " << newton << "\n";
    os << "  numberOfLoadSteps = " << numberOfLoadSteps << "\n";
    os << "  loadStepDuration = " << loadStepDuration << "\n";
    os << "  loadStepStart = " << loadStepStart << "\n";
    os << "  loadStepGeometric = " << loadStepGeometric << "\n";
    os << "  loadStepGeometricRange = " << loadStepGeometricRange << "\n";
    os << "  stabilizerODE2term = " << stabilizerODE2term << "\n";
    os << "  verboseMode = " << verboseMode << "\n";
    os << "  pauseAfterEachStep = " << pauseAfterEachStep << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const StaticSolver& object)
  {
    object.Print(os);
    return os;
  }

};


/** ***********************************************************************************************
* @class        SimulationSettings
* @brief        General Settings for simulation; according settings for solution and solvers are given in subitems of this structure
*
* @author       AUTO: Gerstmayr Johannes
* @date         AUTO: 2019-07-01 (generated)
* @date         AUTO: 2019-12-04 (last modfied)
*
* @copyright    This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See "LICENSE.txt" for more details.
* @note         Bug reports, support and further information:
                - email: johannes.gerstmayr@uibk.ac.at
                - weblink: missing
                
************************************************************************************************ */
#pragma once

#include <ostream>

#include "Utilities/ReleaseAssert.h"
#include "Utilities/BasicDefinitions.h"
#include "System/OutputVariable.h"

class SimulationSettings // AUTO: 
{
protected: // AUTO: 

public: // AUTO: 
  TimeIntegration timeIntegration;                //!< AUTO: time integration parameters
  SolutionSettings solutionSettings;              //!< AUTO: settings for solution files
  StaticSolver staticSolver;                      //!< AUTO: static solver parameters
  LinearSolverType linearSolverType;              //!< AUTO: selection of numerical linear solver: exu.EXUdense (dense matrix inverse), exu.EigenSparseLU (sparse matrix LU-factorization), ... (enumeration type)
  bool displayStatistics;                         //!< AUTO: display general computation information at end of time step (steps, iterations, function calls, step rejections, ...
  bool displayComputationTime;                    //!< AUTO: display computation time at end of time step
  Index outputPrecision;                          //!< AUTO: precision for floating point numbers written to console; e.g. values written by solver
  Index numberOfThreads;                          //!< AUTO: number of threads used for parallel computation (1 == scalar processing); not yet implemented (status: Nov 2019)

  //! AUTO: default constructor with parameter initialization
  SimulationSettings()
  {
    linearSolverType = LinearSolverType::EXUdense;
    displayStatistics = false;
    displayComputationTime = true;
    outputPrecision = 6;
    numberOfThreads = 1;
  };

  // AUTO: access functions
  //! AUTO: clone object; specifically for copying instances of derived class, for automatic memory management e.g. in ObjectContainer
  virtual SimulationSettings* GetClone() const { return new SimulationSettings(*this); }
  
  //! AUTO: Read (Reference) access to: time integration parameters
  const TimeIntegration& GetTimeIntegration() const { return timeIntegration; }
  //! AUTO: Write (Reference) access to: time integration parameters
  TimeIntegration& GetTimeIntegration() { return timeIntegration; }

  //! AUTO: Read (Reference) access to: settings for solution files
  const SolutionSettings& GetSolutionSettings() const { return solutionSettings; }
  //! AUTO: Write (Reference) access to: settings for solution files
  SolutionSettings& GetSolutionSettings() { return solutionSettings; }

  //! AUTO: Read (Reference) access to: static solver parameters
  const StaticSolver& GetStaticSolver() const { return staticSolver; }
  //! AUTO: Write (Reference) access to: static solver parameters
  StaticSolver& GetStaticSolver() { return staticSolver; }

  //! AUTO: Read (Reference) access to: selection of numerical linear solver: exu.EXUdense (dense matrix inverse), exu.EigenSparseLU (sparse matrix LU-factorization), ... (enumeration type)
  const LinearSolverType& GetLinearSolverType() const { return linearSolverType; }
  //! AUTO: Write (Reference) access to: selection of numerical linear solver: exu.EXUdense (dense matrix inverse), exu.EigenSparseLU (sparse matrix LU-factorization), ... (enumeration type)
  LinearSolverType& GetLinearSolverType() { return linearSolverType; }

  //! AUTO: Read (Reference) access to: display general computation information at end of time step (steps, iterations, function calls, step rejections, ...
  const bool& GetDisplayStatistics() const { return displayStatistics; }
  //! AUTO: Write (Reference) access to: display general computation information at end of time step (steps, iterations, function calls, step rejections, ...
  bool& GetDisplayStatistics() { return displayStatistics; }

  //! AUTO: Read (Reference) access to: display computation time at end of time step
  const bool& GetDisplayComputationTime() const { return displayComputationTime; }
  //! AUTO: Write (Reference) access to: display computation time at end of time step
  bool& GetDisplayComputationTime() { return displayComputationTime; }

  //! AUTO: Read (Reference) access to: precision for floating point numbers written to console; e.g. values written by solver
  const Index& GetOutputPrecision() const { return outputPrecision; }
  //! AUTO: Write (Reference) access to: precision for floating point numbers written to console; e.g. values written by solver
  Index& GetOutputPrecision() { return outputPrecision; }

  //! AUTO: Read (Reference) access to: number of threads used for parallel computation (1 == scalar processing); not yet implemented (status: Nov 2019)
  const Index& GetNumberOfThreads() const { return numberOfThreads; }
  //! AUTO: Write (Reference) access to: number of threads used for parallel computation (1 == scalar processing); not yet implemented (status: Nov 2019)
  Index& GetNumberOfThreads() { return numberOfThreads; }

  virtual void Print(std::ostream& os) const
  {
    os << "SimulationSettings" << ":\n";
    os << "  timeIntegration = " << timeIntegration << "\n";
    os << "  solutionSettings = " << solutionSettings << "\n";
    os << "  staticSolver = " << staticSolver << "\n";
    os << "  linearSolverType = " << linearSolverType << "\n";
    os << "  displayStatistics = " << displayStatistics << "\n";
    os << "  displayComputationTime = " << displayComputationTime << "\n";
    os << "  outputPrecision = " << outputPrecision << "\n";
    os << "  numberOfThreads = " << numberOfThreads << "\n";
    os << "\n";
  }

  friend std::ostream& operator<<(std::ostream& os, const SimulationSettings& object)
  {
    object.Print(os);
    return os;
  }

};


