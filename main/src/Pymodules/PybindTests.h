/** ***********************************************************************************************
* @file			PybindTests.h
* @brief		This file contains test functions for pybind11 
* @details		Details:
* 				- put tests here, which are useful for later developements
*
* @author		Gerstmayr Johannes
* @date			2019-05-02 (created)
* @copyright	This file is part of Exudyn. Exudyn is free software: you can redistribute it and/or modify it under the terms of the Exudyn license. See 'LICENSE.txt' for more details.
* @note			Bug reports, support and further information:
* 				- email: johannes.gerstmayr@uibk.ac.at
* 				- weblink: missing
* 				
*
************************************************************************************************ */
#pragma once

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//PYBIND TESTS

#include "Linalg/BasicLinalg.h"

void PythonAlive()
{
	pout << "pout:Exudyn is alive\n";

	//auto d1 = py::dict("str_str"_a = std::string("first name"), "int_str"_a = 2);

	//auto d = py::dict();
	//d["str"] = "asdf";
	//d["integer"] = 123;
	//d["double"] = 12.3;
	//py::print(d);

	//Matrix m1(3, 3, { 1,0,0, 0,1,0, 0,0,1 });
	//Matrix m2(3, 3, { 0,1,0, 1,0,0, 0,0,1 });
	//Matrix m3(3, 3, { 2,1,0, 1,3,4, 0,2,2 });
	//Matrix m4(3, 3, { 0,0,7, 2,1,4, 4,0,0 });

	//pout << "m1=" << m1 << "\n";
	//pout << "m2=" << m2 << "\n";
	//pout << "m3=" << m3 << "\n";
	//pout << "m4=" << m4 << "\n";

	//m1.Invert();
	//m2.Invert();
	//m3.Invert();
	//m4.Invert();

	//pout << "m1inv=" << m1 << "\n";
	//pout << "m2inv=" << m2 << "\n";
	//pout << "m3inv=" << m3 << "\n";
	//pout << "m4inv=" << m4 << "\n";

	//m1.Invert();
	//m2.Invert();
	//m3.Invert();
	//m4.Invert();

	//pout << "m1=" << m1 << "\n";
	//pout << "m2=" << m2 << "\n";
	//pout << "m3=" << m3 << "\n";
	//pout << "m4=" << m4 << "\n";

	
	EXUmath::MatrixTests(); //perform matrix inverse tests
}

void PythonGo()
{
	//these commands directly go into the current interpreter ... (TODO: check, if PythonGo() is called from an interpreter ..., otherwise crashes probably)
	//py::exec(R"(
	//	systemContainer = exu.SystemContainer()
	//	mbs = systemContainer.AddSystem()
	//	node = {'nodeType': 'Point', 'referenceCoordinates': [1.0, 0.0, 0.0], 'initialDisplacements': [0.0, 2.0, 0.0], 'initialVelocities': [0.0, 3.0, 0.0], 'name': 'node one'}
	//	mbs.AddNode(node)
	//	massPoint = {'objectType': 'MassPoint', 
	//			     'physicsMass': 3.0, 
	//				 'nodeNumber': 0, 
	//	             'name': 'first masspoint'}
	//	mbs.AddObject(massPoint)
 //   )");
	py::exec(R"(
		systemContainer = exu.SystemContainer()
		mbs = systemContainer.AddSystem()
    )");
	pout << "main variables:\n systemContainer=exu.SystemContainer()\n mbs = systemContainer.AddSystem()\n";
	//pout << "ready to go\n";
}

void PythonCreateSys10()
{
	py::exec(R"(
		systemContainer = ht.SystemContainer()
		mbs = systemContainer.AddSystem()
		for i in range(10): mbs.AddNode({'nodeType': 'Point','referenceCoordinates': [i, 0.0, 0.0],'initialDisplacements': [0.0, 0.0, (i+1)/10], 'initialVelocities': [0.0, i*2, 0.0],})
		for i in range(10): mbs.AddObject({'objectType': 'MassPoint', 'physicsMass': 20-i, 'nodeNumber': i})
		for i in range(10): mbs.AddMarker({'markerType': 'BodyPosition',  'bodyNumber': i,  'localPosition': [0.0, 0.0, 0.0], 'bodyFixed': False})
		for i in range(9): mbs.AddObject({'objectType': 'SpringDamper', 'stiffness': 4, 'damping': 0, 'force': 0, 'referenceLength':1, 'markerNumbers': [i,i+1]})		
		mbs
    )");
	//pout << "ready to go\n";
}

py::object GetIndex()
{
	return py::int_(42);
}

py::object GetString()
{
	return py::str("A string");
}

py::object GetReal()
{
	return py::float_(double(42.1234567890123456)); //precision maintained in python: 42.123456789012344
}

py::object GetVector()
{
	Vector v({ 42.1234567890123456,43,44 }); //double precision maintained in NumPy array in python
	return py::array_t<Real>(v.NumberOfItems(), v.GetDataPointer()); //copy array (could also be referenced!)
}

py::object GetOutputVariableType() //as OutputVariableType is known, it can return the special type
{
	return py::cast(OutputVariableType::Position);
}

////Exudyn-access to test Vector
//const Vector& GetTest() { return test; }
//void SetTest(const Vector& vector) { test = vector; }

//Numpy-access to test Vector
//py::array_t<Real> GetNumpyTest() { return py::array_t<Real>(test.NumberOfItems(), test.GetDataPointer()); }
//void SetNumpyTest(const std::vector<Real>& v) { test = v; }
