#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# This is the test suite which shall serve as a general driver to run:
#   - model unit tests
#   - test examples (e.g. from example folder)
#   - internal EXUDYN unit tests
#
# Author:   Johannes Gerstmayr
# Date:     2019-11-01
#
# Copyright:This file is part of Exudyn. Exudyn is free software. You can redistribute it and/or modify it under the terms of the Exudyn license. See 'LICENSE.txt' for more details.
#
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import sys
sys.path.append('../bin/WorkingRelease') #for exudyn, itemInterface and exudynUtilities
sys.path.append('TestModels')            #for modelUnitTest as this example may be used also as a unit test

import exudyn as exu
from modelUnitTests import RunAllModelUnitTests, TestInterface, ExudynTestStructure, exudynTestGlobals

print('EXUDYN version='+exu.__version__)

SC = exu.SystemContainer()
mbs = SC.AddSystem()

testInterface = TestInterface(exudyn = exu, systemContainer = SC, useGraphics=False)
rv = RunAllModelUnitTests(mbs, testInterface)


testFileList = ['Examples/fourBarMechanism.py', 
                'Examples/sparseMatrixSpringDamperTest.py',
                'Examples/springDamperUserFunctionTest.py',
                'Examples/ANCF_contact_circle_test.py']
#testFileList = ['Examples/fourBarMechanism.py']

totalTests = len(testFileList)
testsFailed = [] #list of numbers containing the test numbers of failed tests
exudynTestGlobals.useGraphics = False

cnt = 0
for file in testFileList:
    exudynTestGlobals.testError = -1 #default value !=-1, if there is an error in the calculation
    exec(open(file).read(), globals())
    if exudynTestGlobals.testError == 0:
        print('******************************************')
        print('  EXAMPLE ' + str(cnt) + ' ("' + file + '") FINISHED SUCCESSFUL')
        print('******************************************')
    else:
        print('******************************************')
        print('  EXAMPLE ' + str(cnt) + ' ("' + file + '") FAILED')
        print('  ERROR = ' + str(exudynTestGlobals.testError))
        print('******************************************')
        testsFailed = testsFailed + [cnt]
    
    cnt += 1
        
        
print('\n\n******************************************')
print('TEST SUITE RESULTS SUMMARY:')
print('******************************************')
        
if rv == True:
    print('ALL UNIT TESTS SUCCESSFUL')
else:
    print('UNIT TESTS FAILED: see above section UNIT TESTS for detailed information')
    
if len(testsFailed) == 0:
    print('ALL ' + str(totalTests) + ' EXAMPLE TESTS SUCCESSFUL')
else:
    print(str(len(testsFailed)) + ' EXAMPLE TEST(S) OUT OF '+ str(totalTests) + ' FAILED: ')
    for i in testsFailed:
        print('  EXAMPLE ' + str(i) + ' (' + testFileList[i] + ') FAILED')
        
