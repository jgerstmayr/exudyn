#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# This is an EXUDYN example
#
# Details:  Slider crank model with verification in MATLAB for machine dynamics course
#           optionally, the slider crank is mounted on a floating frame, leading to vibrations
#           if the system is unbalanced
#           This example features 3D graphics of the links
#
# Author:   Johannes Gerstmayr
# Date:     2019-12-07
#
# Copyright:This file is part of Exudyn. Exudyn is free software. You can redistribute it and/or modify it under the terms of the Exudyn license. See 'LICENSE.txt' for more details.
#
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import sys
sys.path.append('../../bin/WorkingRelease') #for exudyn, itemInterface and exudynUtilities
#sys.path.append('../TestModels')            #for modelUnitTest as this example may be used also as a unit test

import exudyn as exu
from exudynUtilities import *
from itemInterface import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

SC = exu.SystemContainer()
mbs = SC.AddSystem()

#++++++++++++++++++++++++++++++++
#slider-crank
# test nonlinear model; index2 and index3-formulation for ConnectorCoordinate and RevoluteJoint2D
# crank is mounted at (0,0,0); crank length = 2*a0, connecting rod length = 2*a1

#++++++++++++++++++++++++++++++++
#ground object/node:

background = GraphicsDataRectangle(-0.5, -0.5, 1, 0.5, color=[1,1,1,1.]) #invisible background
#background2 = GraphicsDataOrthoCube(-1, -1, -1, 2, -0.8, -0.8, color=[0.3,0.5,0.5,1.]) 
background2 = GraphicsDataCylinder(pAxis=[0,0.5,0],vAxis=[0,0,1],radius=0.3, color=[0.3,0.5,0.5,1.], 
                                   nTiles=16, angleRange=[0,np.pi*1.2], lastFace=True, cutPlain=True) 

background2 = GraphicsDataSphere(point=[0,0.5,0],radius=0.3,color=[0.3,0.5,0.5,1.],nTiles=8)

background2 = GraphicsDataRigidLink(p0=[0,0.5,0],p1=[1,0.5,0], axis0=[0,0,1], axis1=[0,0,1],
                                    radius=[0.1,0.1],thickness=0.2, width=[0.2,0.2],color=[0.3,0.5,0.5,1.],nTiles=16)
#print(background1)

oGround=mbs.AddObject(ObjectGround(referencePosition= [0,0,0], 
                                   visualization=VObjectGround(graphicsData= [background])))
nGround = mbs.AddNode(NodePointGround(referenceCoordinates=[0,0,0])) #ground node for coordinate constraint

#++++++++++++++++++++++++++++++++
#nodes and bodies
omega=2*np.pi/60*300 #3000 rpm
L1=0.1
L2=0.3
s1=L1*0.5
s2=L2*0.5
m1=0.2
m2=0.2
m3=0.4
M=0.1 #torque
#lambda=L1/L2
J1=(m1/12.)*L1**2 #inertia w.r.t. center of mass
J2=(m2/12.)*L2**2 #inertia w.r.t. center of mass

ty = 0.05    #thickness
tz = 0.05    #thickness
#graphics1 = GraphicsDataRectangle(-0.5*L1,-0.5*ty,0.5*L1,0.5*ty,color4steelblue)
#graphics1 = GraphicsDataOrthoCube(-0.5*L1,-0.5*ty,-tz,0.5*L1,0.5*ty,0,color4steelblue)
graphics1 = GraphicsDataRigidLink(p0=[-0.5*L1,0,-0.5*tz],p1=[0.5*L1,0,-0.5*tz], 
                                  axis0=[0,0,1], axis1=[0,0,1],radius=[0.5*ty,0.5*ty],
                                  thickness=0.8*ty, width=[tz,tz], color=color4steelblue,nTiles=16)

#graphics2 = GraphicsDataRectangle(-0.5*L2,-0.5*ty,0.5*L2,0.5*ty,color4lightred)
#graphics2 = GraphicsDataOrthoCube(-0.5*L2,-0.5*ty,0,0.5*L2,0.5*ty,tz,color4lightred)
graphics2 = GraphicsDataRigidLink(p0=[-0.5*L2,0,0.5*tz],p1=[0.5*L2,0,0.5*tz], 
                                  axis0=[0,0,1], axis1=[0,0,1],radius=[0.5*ty,0.5*ty],
                                  thickness=0.8*ty, width=[tz,tz], color=color4lightred,nTiles=16)

use3D=False
if use3D:
    nRigid1 = mbs.AddNode(Rigid3DEP(referenceCoordinates=[s1,0,0,1,0,0,0], 
                                  initialVelocities=[0,0,0,0,0,0,0]));
    oRigid1 = mbs.AddObject(RigidBody(physicsMass=m1, 
                                        physicsInertia=[J1,J1,J1,0,0,0],
                                        nodeNumber=nRigid1,
                                        visualization=VObjectRigidBody(graphicsData= [graphics1])))
    nRigid2 = mbs.AddNode(Rigid3DEP(referenceCoordinates=[L1+s2,0,0,1,0,0,0], 
                                  initialVelocities=[0,0,0,0,0,0,0]));
    oRigid2 = mbs.AddObject(RigidBody(physicsMass=m2, 
                                        physicsInertia=[J2,J2,J2,0,0,0],
                                        nodeNumber=nRigid2,
                                        visualization=VObjectRigidBody(graphicsData= [graphics2])))
else:
    nRigid1 = mbs.AddNode(Rigid2D(referenceCoordinates=[s1,0,0], 
                                  initialVelocities=[0,0,0]));
    oRigid1 = mbs.AddObject(RigidBody2D(physicsMass=m1, 
                                        physicsInertia=J1,
                                        nodeNumber=nRigid1,
                                        visualization=VObjectRigidBody2D(graphicsData= [graphics1])))
    
    nRigid2 = mbs.AddNode(Rigid2D(referenceCoordinates=[L1+s2,0,0], 
                                  initialVelocities=[0,0,0]));
    oRigid2 = mbs.AddObject(RigidBody2D(physicsMass=m2, 
                                        physicsInertia=J2,
                                        nodeNumber=nRigid2,
                                        visualization=VObjectRigidBody2D(graphicsData= [graphics2])))


c=0.05 #dimension of mass
#graphics3 = GraphicsDataRectangle(-c,-c,c,c,color4grey)
graphics3 = GraphicsDataOrthoCube(-c,-c,-c,c,c,c,color4grey)

nMass = mbs.AddNode(Point2D(referenceCoordinates=[L1+L2,0]))
oMass = mbs.AddObject(MassPoint2D(physicsMass=m3, nodeNumber=nMass,visualization=VObjectRigidBody2D(graphicsData= [graphics3])))

#++++++++++++++++++++++++++++++++
#markers for joints:
mR1Left = mbs.AddMarker(MarkerBodyRigid(bodyNumber=oRigid1, localPosition=[-s1,0.,0.])) #support point # MUST be a rigidBodyMarker, because a torque is applied
mR1Right = mbs.AddMarker(MarkerBodyPosition(bodyNumber=oRigid1, localPosition=[ s1,0.,0.])) #end point; connection to connecting rod

mR2Left = mbs.AddMarker(MarkerBodyPosition(bodyNumber=oRigid2, localPosition=[-s2,0.,0.])) #connection to crank
mR2Right = mbs.AddMarker(MarkerBodyPosition(bodyNumber=oRigid2, localPosition=[ s2,0.,0.])) #end point; connection to slider

mMass = mbs.AddMarker(MarkerBodyPosition(bodyNumber=oMass, localPosition=[ 0.,0.,0.]))
mG0 = mbs.AddMarker(MarkerBodyPosition(bodyNumber=oGround, localPosition=[0,0,0.]))

#++++++++++++++++++++++++++++++++
#joints:
mbs.AddObject(RevoluteJoint2D(markerNumbers=[mG0,mR1Left]))
mbs.AddObject(RevoluteJoint2D(markerNumbers=[mR1Right,mR2Left]))
mbs.AddObject(RevoluteJoint2D(markerNumbers=[mR2Right,mMass]))

#++++++++++++++++++++++++++++++++
#markers for node constraints:
mGround = mbs.AddMarker(MarkerNodeCoordinate(nodeNumber = nGround, coordinate=0)) #Ground node ==> no action
mNodeSlider = mbs.AddMarker(MarkerNodeCoordinate(nodeNumber = nMass, coordinate=1)) #y-coordinate is constrained

#++++++++++++++++++++++++++++++++
#coordinate constraints for slider (free motion in x-direction)
mbs.AddObject(CoordinateConstraint(markerNumbers=[mGround,mNodeSlider]))

#user function for load; switch off load after 1 second
def userLoad(t, load):
    if t <= 2: return load
    return 0

#loads and driving forces:
mRigid1CoordinateTheta = mbs.AddMarker(MarkerNodeCoordinate(nodeNumber = nRigid1, coordinate=2)) #angle coordinate is constrained
mbs.AddLoad(LoadCoordinate(markerNumber=mRigid1CoordinateTheta, load = M, loadUserFunction=userLoad)) #torque at crank
#mbs.AddLoad(Torque(markerNumber = mR1Left, loadVector = [0, 0, M])) #apply torque at crank

#++++++++++++++++++++++++++++++++
#assemble, adjust settings and start time integration
mbs.Assemble()

simulationSettings = exu.SimulationSettings() #takes currently set values or default values

simulationSettings.timeIntegration.numberOfSteps = 2*25000 #1000 steps for test suite/error
simulationSettings.timeIntegration.endTime = 3              #1s for test suite / error
#simulationSettings.timeIntegration.newton.relativeTolerance = 1e-8 #10000
simulationSettings.timeIntegration.verboseMode = 1 #10000

simulationSettings.solutionSettings.solutionWritePeriod = 2e-4
simulationSettings.timeIntegration.newton.useModifiedNewton = True
simulationSettings.timeIntegration.newton.relativeTolerance = 1e-8
simulationSettings.timeIntegration.newton.absoluteTolerance = 1e-2
#simulationSettings.timeIntegration.generalizedAlpha.useNewmark = True
#simulationSettings.timeIntegration.generalizedAlpha.useIndex2Constraints = True
simulationSettings.timeIntegration.generalizedAlpha.spectralRadius = 0.5


exu.StartRenderer()
#mbs.WaitForUserToContinue()
#++++++++++++++++++++++++++++++++++++++++++
#solve index 2 / trapezoidal rule:
simulationSettings.timeIntegration.generalizedAlpha.useNewmark = True
simulationSettings.timeIntegration.generalizedAlpha.useIndex2Constraints = True

#mbs.WaitForUserToContinue()
SC.TimeIntegrationSolve(mbs, 'GeneralizedAlpha', simulationSettings)

##animate solution
fileName = 'coordinatesSolution.txt'
solution = LoadSolutionFile('coordinatesSolution.txt')
AnimateSolution(exu, SC, mbs, solution, 10, 0.025)
    
exu.StopRenderer() #safely close rendering window!

u = mbs.GetNodeOutput(nMass, exu.OutputVariableType.Position) #tip node
errorSliderCrankIndex2 = u[0] - 1.3528786319585837 #x-position of slider
print('error errorSliderCrankIndex2=',errorSliderCrankIndex2)

plotResults = False
if plotResults:
    dataIndex2 = np.loadtxt('coordinatesSolution.txt', comments='#', delimiter=',')
    #dataMatlab = np.loadtxt('slidercrankRefSolM0.1_tol1e-4.txt', comments='#', delimiter=',') #this is quite inaccurate
    dataMatlab2 = np.loadtxt('slidercrankRefSolM0.1_tol1e-6.txt', comments='#', delimiter=',')
                            
    vODE2=mbs.systemData.GetODE2Coordinates()
    nODE2=len(vODE2) #number of ODE2 coordinates
    
    plt.plot(dataIndex2[:,0], dataIndex2[:,3], 'b-') #plot angle of crank
    plt.plot(dataIndex2[:,0], dataIndex2[:,nODE2+3], 'r-') #plot angular velocity of crank
    #plt.plot(dataMatlab[:,0], dataMatlab[:,2], 'g-') #plot angular velocity of crank from MATLAB
    plt.plot(dataMatlab2[:,0], dataMatlab2[:,2], 'k-') #plot angular velocity of crank from MATLAB
    
    #plt.plot(dataIndex3[:,0], dataIndex3[:,1+globalIndex], 'b-') #plot x-coordinate of slider
    
    ax=plt.gca() # get current axes
    ax.grid(True, 'major', 'both')
    ax.xaxis.set_major_locator(ticker.MaxNLocator(10)) #use maximum of 8 ticks on y-axis
    ax.yaxis.set_major_locator(ticker.MaxNLocator(10)) #use maximum of 8 ticks on y-axis
    plt.tight_layout()
    plt.show() 
    
